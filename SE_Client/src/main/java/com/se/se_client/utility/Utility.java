package com.se.se_client.utility;

import com.se.se_client.service.ServiziImpl;
import com.se.se_client.beans.Response;
import com.se.se_client.ui.MainPage;
import java.awt.Dimension;
import java.awt.Toolkit;
import javax.swing.JFrame;
import javax.swing.JOptionPane;

/**
 *
 * @author Gugliandolo & Infortuna
 */
public class Utility {
    public static final Double  INFINITY = -999.99999d;
    private static final ServiziImpl servizi = new ServiziImpl();
    
    public static void checkSession(JFrame frame, Response resp) {
        checkNull(frame);
        
        if(resp.getStatoLogin().equals(Response.StatoLogin.LOGGATO)) 
            return;
        
        JOptionPane.showMessageDialog(frame, "Sessione scaduta");
        goToMainPage(frame);
    }
    
    public static void goToMainPage(JFrame frame) {
        Sessione.closeSession();
        MainPage login = new MainPage();
        login.setVisible(true);
        if(frame != null) {
            login.setLocation(frame.getLocation());
            frame.dispose();
        } else {
            Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();
            login.setLocation(dim.width/2-login.getSize().width/2, dim.height/2-login.getSize().height/2);
        }
    }
    
    public static void goTo(JFrame from, JFrame to) {
        checkNull(from);
        checkNull(to);
        
        to.setVisible(true);
        to.setLocation(from.getLocation());
        from.dispose();
    }
    
    public static void checkNull(JFrame frame) {
        if(frame != null)
            return;
        JOptionPane.showMessageDialog(null, "Si è verificato un errore imprevisto");
        goToMainPage(frame);
    }

    public static ServiziImpl getServizi() {
        return servizi;
    }

}
