package com.se.se_client.utility;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.util.Map;
import org.apache.commons.codec.Charsets;
import org.apache.http.Header;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.util.EntityUtils;

/**
 *
 * @author Gugliandolo & Infortuna
 */
public class HTTPClient {
    private static final String SERVER_URL = "http://localhost:8080/";
    private static final Gson gson = new GsonBuilder().setDateFormat("EEE, dd MMM yyyy HH:mm:ss zzz").create();
    
    public static  <T> Object sendJsonRequest(
            String serviceName, Object input, Class<T> responseClass
    ) throws IOException {
        HttpClient httpClient = HttpClientBuilder.create().build();
        HttpPost request = new HttpPost(SERVER_URL + serviceName);
        StringEntity requestEntity = new StringEntity(
                gson.toJson(input), ContentType.APPLICATION_JSON
        );
        request.addHeader("content-type", "application/json");
        request.setEntity(requestEntity);
        HttpResponse resp = httpClient.execute(request);
        HttpEntity entity = resp.getEntity();
        Header encodingHeader = entity.getContentEncoding();
        Charset encoding = encodingHeader == null ? StandardCharsets.UTF_8 : 
        Charsets.toCharset(encodingHeader.getValue());
        String jsonResponse = EntityUtils.toString(entity, encoding);

        return gson.fromJson(jsonResponse, responseClass);
    }

    public static  <T> Object sendGetRequest(
            String url, Class<T> responseClass, Map<String, String> headers
    ) throws IOException {
        HttpClient httpClient = HttpClientBuilder.create().build();
        HttpGet request = new HttpGet(url);
        
        request.addHeader("content-type", "application/json");
        if(headers != null && !headers.isEmpty()) {
            for(String chiave : headers.keySet()) {
                request.addHeader(chiave, headers.get(chiave));
            }
        }
        HttpResponse resp = httpClient.execute(request);
        HttpEntity entity = resp.getEntity();
        Header encodingHeader = entity.getContentEncoding();
        Charset encoding = encodingHeader == null ? StandardCharsets.UTF_8 : 
        Charsets.toCharset(encodingHeader.getValue());
        String jsonResponse = EntityUtils.toString(entity, encoding);

        return gson.fromJson(jsonResponse, responseClass);
    }
        
    public static Gson getGsonInstance() {
        return gson;
    }

}
