package com.se.se_client.utility;

/**
 *
 * @author Gugliandolo & Infortuna
 */
public class Sessione {
    private static Long idSessione;
    private static String nomeUtente;
    private static String email;
    private static Boolean utenteConOTP;
    
    public static Long getIdSessione() {
        return idSessione;
    }

    public static void setIdSessione(Long idSessione) {
        if(Sessione.idSessione != null)
            return;
        Sessione.idSessione = idSessione;
    }

    public static String getNomeUtente() {
        return nomeUtente;
    }

    public static void setNomeUtente(String nomeUtente) {
        if(Sessione.nomeUtente != null)
            return;
        Sessione.nomeUtente = nomeUtente;
    }

    public static String getEmail() {
        return email;
    }

    public static void setEmail(String email) {
        if(Sessione.email != null)
            return;
        Sessione.email = email;
    }
    
    public static boolean isUtenteConOTP() {
        if(utenteConOTP == null)
            return false;
        return utenteConOTP;
    }

    public static void setUtenteConOTP(boolean utenteConOTP) {
        Sessione.utenteConOTP = utenteConOTP;
    }

    protected static void closeSession() {
        idSessione = null;
        nomeUtente = null;
        email = null;
        utenteConOTP = null;
    }
}
