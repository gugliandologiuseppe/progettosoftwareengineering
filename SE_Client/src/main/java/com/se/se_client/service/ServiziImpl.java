package com.se.se_client.service;

import com.google.gson.Gson;
import com.google.gson.stream.JsonReader;
import com.se.se_client.utility.HTTPClient;
import com.se.se_client.beans.AggiornaEmailInput;
import com.se.se_client.beans.AggiornaEmailResponse;
import com.se.se_client.beans.AggiornaPasswordInput;
import com.se.se_client.beans.AggiornaPasswordResponse;
import com.se.se_client.beans.CheckpointData;
import com.se.se_client.beans.GetBestScoresInput;
import com.se.se_client.beans.GetBestScoresResponse;
import com.se.se_client.beans.LoginInput;
import com.se.se_client.beans.LoginResponse;
import com.se.se_client.beans.LogoutInput;
import com.se.se_client.beans.LogoutResponse;
import com.se.se_client.beans.RegistrationInput;
import com.se.se_client.beans.RegistrationResponse;
import com.se.se_client.beans.UpdateCheckpointInput;
import com.se.se_client.beans.UpdateCheckpointReponse;
import com.se.se_client.game.CheckpointData2;
import com.se.se_client.utility.Sessione;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;
import java.security.NoSuchAlgorithmException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Gugliandolo & Infortuna
 */
public class ServiziImpl {
    final String jsonFileUrl = "datiCheckpoint.json";
    
    private  com.se.se_client.beans.CheckpointData checkpoint = new  com.se.se_client.beans.CheckpointData();
    
    public RegistrationResponse doRegistration(RegistrationInput input) throws IOException, NoSuchAlgorithmException {
        return (RegistrationResponse) HTTPClient.sendJsonRequest("RegistrationService", input, RegistrationResponse.class);
    }

    public LoginResponse doLogin(LoginInput input) throws IOException, NoSuchAlgorithmException {
        return (LoginResponse) HTTPClient.sendJsonRequest("LoginService", input, LoginResponse.class);
    }
    
    public LogoutResponse doLogout(LogoutInput input) throws IOException {
        return (LogoutResponse) HTTPClient.sendJsonRequest("LogoutService", input, LogoutResponse.class);
    }

    public AggiornaEmailResponse aggiornaEmail(AggiornaEmailInput input) throws IOException {
        return (AggiornaEmailResponse) HTTPClient.sendJsonRequest("EmailUpdate", input, AggiornaEmailResponse.class);
    }

    public AggiornaPasswordResponse aggiornaPassword(AggiornaPasswordInput input) throws IOException {
        return (AggiornaPasswordResponse) HTTPClient.sendJsonRequest("PasswordUpdate", input, AggiornaPasswordResponse.class);
    }
    
    public GetBestScoresResponse getBestScores(GetBestScoresInput input) throws IOException {
        return (GetBestScoresResponse) HTTPClient.sendJsonRequest("getScores", input, GetBestScoresResponse.class);
    }

    public void updateCheckpoint() {
        try {
            File tempFile = new File(jsonFileUrl);
            if(!tempFile.exists()) {
                return;
            }
            
            Gson gson = new Gson();
            JsonReader reader = new JsonReader(new FileReader(jsonFileUrl));
            CheckpointData2 readCase = gson.fromJson(reader, CheckpointData2.class);
            System.out.println(readCase);
            if(readCase.score == null || (checkpoint.getScore() != null && readCase.score.intValue() == checkpoint.getScore())) {
                return;
            }
            
            checkpoint = new  com.se.se_client.beans.CheckpointData(readCase.monete, readCase.collezionabili, readCase.checkpoint, readCase.speed, readCase.attackDamage, readCase.shotDamage, readCase.attackSpeed, readCase.life, readCase.choiceShot, readCase.score);
            
            updateCheckpoint(new UpdateCheckpointInput(Sessione.getIdSessione(), checkpoint));
            System.out.println(gson.toJson(new UpdateCheckpointInput(Sessione.getIdSessione(), checkpoint)));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private UpdateCheckpointReponse updateCheckpoint(UpdateCheckpointInput input) throws IOException {
        return (UpdateCheckpointReponse) HTTPClient.sendJsonRequest("UpdateCheckpoint", input, UpdateCheckpointReponse.class);
    }

    public void clearJsonFile() {
        File tempFile = new File(jsonFileUrl);
        if(!tempFile.exists()) {
            return;
        }
        
        tempFile.delete();
    }
    
    public void saveCheckpointData(CheckpointData checkpointData) {
        try {
            
            if(checkpointData == null || checkpointData.getScore() == null) {
                return;
            }
            Gson gson = new Gson();
            try (Writer writer = new FileWriter(jsonFileUrl)) {
                gson.toJson(checkpointData, writer);
                writer.flush();
            }
        } catch (IOException ex) {
            Logger.getLogger(ServiziImpl.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
}
