package com.se.se_client.beans;

/**
 *
 * @author Gugliandolo & Infortuna
 */
public class GetBestScoresInput extends Input {

    public GetBestScoresInput() {
        super();
    }

    public GetBestScoresInput(long idSessione) {
        super(idSessione);
    }

}
