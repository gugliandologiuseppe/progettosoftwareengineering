package com.se.se_client.beans;

/**
 *
 * @author Gugliandolo & Infortuna
 */
public class LogoutInput extends Input{

    public LogoutInput() {
        super();
    }
    
    public LogoutInput(long idSessione) {
        super(idSessione);
    }

}
