package com.se.se_client.beans;

import com.google.gson.annotations.Expose;

/**
 *
 * @author Gugliandolo & Infortuna
 */
public class UpdateCheckpointInput extends Input {
    @Expose
    private CheckpointData dati;

    public UpdateCheckpointInput() {
        super();
    }

    public UpdateCheckpointInput(long idSessione, CheckpointData dati) {
        super(idSessione);
        this.dati = dati;
    }

    public CheckpointData getCheckpointData() {
        return dati;
    }

    public void setCheckpointData(CheckpointData dati) {
        this.dati = dati;
    }
    
}
