package com.se.se_client.beans;

import com.google.gson.annotations.Expose;

/**
 *
 * @author Gugliandolo & Infortuna
 */
public abstract class Input {
    @Expose
    private long idSessione;

    public Input() {
    }

    public Input(long idSessione) {
        this.idSessione = idSessione;
    }

    public final long getIdSessione() {
        return idSessione;
    }

    public final void setIdSessione(long idSessione) {
        this.idSessione = idSessione;
    }
    
}
