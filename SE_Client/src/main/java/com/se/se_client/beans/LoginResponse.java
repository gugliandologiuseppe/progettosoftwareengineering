package com.se.se_client.beans;

import com.google.gson.annotations.Expose;

/**
 *
 * @author Gugliandolo & Infortuna
 */
public class LoginResponse extends Response {
    @Expose
    private Long idSessione; //2005 forse 2009
    
    @Expose
    private String nomeUtente;
    
    @Expose
    private CheckpointData checkpointData;
    
    public LoginResponse() {
        super();
    }

    public LoginResponse(StatoLogin statoLogin) {
        super(statoLogin);
    }

    public LoginResponse(StatoLogin statoLogin, Long idSessione, String nomeUtente, CheckpointData checkpointData) {
        super(statoLogin);
        this.idSessione = idSessione;
        this.nomeUtente = nomeUtente;
        this.checkpointData = checkpointData;
    }

    public Long getIdSessione() {
        return idSessione;
    }

    public void setIdSessione(Long idSessione) {
        this.idSessione = idSessione;
    }

    public String getNomeUtente() {
        return nomeUtente;
    }

    public void setNomeUtente(String nomeUtente) {
        this.nomeUtente = nomeUtente;
    }

    public CheckpointData getCheckpointData() {
        return checkpointData;
    }

    public void setCheckpointData(CheckpointData checkpointData) {
        this.checkpointData = checkpointData;
    }

}
