package com.se.se_client.beans;

import com.google.gson.annotations.Expose;

/**
 *
 * @author Gugliandolo & Infortuna
 */
public class AggiornaEmailInput extends Input {
    @Expose
    private String email;

    public AggiornaEmailInput() {
        super();
    }

    public AggiornaEmailInput(long idSessione, String email) {
        super(idSessione);
        this.email = email;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
    
}
