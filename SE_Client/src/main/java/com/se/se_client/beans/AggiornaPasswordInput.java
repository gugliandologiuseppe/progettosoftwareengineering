package com.se.se_client.beans;

import com.google.gson.annotations.Expose;

/**
 *
 * @author Gugliandolo & Infortuna
 */
public class AggiornaPasswordInput extends Input {
    @Expose
    private String password;
    
    @Expose
    private String oldPassword;

    public AggiornaPasswordInput() {
        super();
    }

    public AggiornaPasswordInput(long idSessione, String password, String oldPassword) {
        super(idSessione);
        this.password = password;
        this.oldPassword = oldPassword;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getPassword() {
        return password;
    }

    public String getOldPassword() {
        return oldPassword;
    }

    public void setOldPassword(String oldPassword) {
        this.oldPassword = oldPassword;
    }
    
}
