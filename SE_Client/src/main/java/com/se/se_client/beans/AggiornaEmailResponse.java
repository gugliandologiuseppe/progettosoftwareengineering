package com.se.se_client.beans;

import com.google.gson.annotations.Expose;

/**
 *
 * @author Gugliandolo & Infortuna
 */
public class AggiornaEmailResponse extends Response {
    @Expose
    private Boolean operazioneEseguita;

    public AggiornaEmailResponse() {
        super();
    }

    public AggiornaEmailResponse(StatoLogin statoLogin) {
        super(statoLogin);
    }
    
    public AggiornaEmailResponse(StatoLogin statoLogin, boolean esito) {
        super(statoLogin);
        this.operazioneEseguita = esito;
    }

    public boolean isOperazioneEseguita() {
        return operazioneEseguita;
    }

    public void setOperazioneEseguita(boolean esito) {
        this.operazioneEseguita = esito;
    }
    
}
