package com.se.se_client.beans;

import com.google.gson.annotations.Expose;

/**
 *
 */
public class UpdateCheckpointReponse extends Response {
    @Expose
    private String esito;

    public UpdateCheckpointReponse() {
        super();
    }

    public UpdateCheckpointReponse(StatoLogin statoLogin) {
        super(statoLogin);
    }

    public UpdateCheckpointReponse(StatoLogin statoLogin, String esito) {
        super(statoLogin);
        this.esito = esito;
    }

    public String getEsito() {
        return esito;
    }

    public void setEsito(String esito) {
        this.esito = esito;
    }
    
}
