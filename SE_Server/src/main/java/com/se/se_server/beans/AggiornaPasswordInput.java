package com.se.se_server.beans;

/**
 *
 * @author Gugliandolo & Infortuna
 */
public class AggiornaPasswordInput extends Input {
    private String password;
    
    private String oldPassword;

    public AggiornaPasswordInput() {
        super();
    }

    public AggiornaPasswordInput(long idSessione, String password, String oldPassword) {
        super(idSessione);
        this.password = password;
        this.oldPassword = oldPassword;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getPassword() {
        return password;
    }

    public String getOldPassword() {
        return oldPassword;
    }

    public void setOldPassword(String oldPassword) {
        this.oldPassword = oldPassword;
    }
    
}
