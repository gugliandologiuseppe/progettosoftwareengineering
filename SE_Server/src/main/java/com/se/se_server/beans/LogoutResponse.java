package com.se.se_server.beans;

/**
 *
 * @author Gugliandolo & Infortuna
 */
public class LogoutResponse {
    private String response;

    private LogoutResponse() {
    }

    public LogoutResponse(String response) {
        this.response = response;
    }

    public String getResponse() {
        return response;
    }

    public void setResponse(String response) {
        this.response = response;
    }
    
}
