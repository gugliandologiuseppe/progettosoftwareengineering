package com.se.se_server.beans;

/**
 *
 * @author Gugliandolo & Infortuna
 */
public class GetBestScoresInput extends Input {

    public GetBestScoresInput() {
        super();
    }

    public GetBestScoresInput(long idSessione) {
        super(idSessione);
    }

}
