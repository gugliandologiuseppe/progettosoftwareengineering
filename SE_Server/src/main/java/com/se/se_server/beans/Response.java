package com.se.se_server.beans;

/**
 *
 * @author Gugliandolo & Infortuna
 */
public abstract class Response {
    public enum StatoLogin {
        SESSIONE_SCADUTA(-2l),
        SLOGGATO(-1l),
        LOGGATO(1l),
        PASSWORD_SCADUTA(2l),
        LIMITE_ERRORI_PASS,
        CREDENZIALI_ERRATE;
        
        public final Long value;
        
        private StatoLogin() {
            value = null;
        }
        
        private StatoLogin(Long value) {
            this.value = value;
        }
        
        public Long getValue() {
            return value;
        }
    }

    public Response() {
    }

    public Response(StatoLogin statoLogin) {
        this.statoLogin = statoLogin;
    }
    
    private StatoLogin statoLogin;
    
    public final StatoLogin getStatoLogin() {
        return statoLogin;
    }

    public final void setStatoLogin(StatoLogin statoLogin) {
        this.statoLogin = statoLogin;
    }
    
}
