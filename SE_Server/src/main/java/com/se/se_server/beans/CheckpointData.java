package com.se.se_server.beans;

/**
 *
 * @author Gugliandolo & Infortuna
 */
public class CheckpointData {
    private Integer monete;
    private Integer collezionabili;
    private String checkpoint;
    private Double speed;
    private Integer attackDamage;
    private Integer shotDamage;
    private Double attackSpeed;
    private Double life;
    private Integer choiceShot; 
    private Integer score; 
    
    public CheckpointData() {
    }

    public CheckpointData(Integer monete, Integer collezionabili, String checkpoint, Double speed, Integer attackDamage, Integer shotDamage, Double attackSpeed, Double life, Integer choiceShot, Integer score) {
        this.monete = monete;
        this.collezionabili = collezionabili;
        this.checkpoint = checkpoint;
        this.speed = speed;
        this.attackDamage = attackDamage;
        this.shotDamage = shotDamage;
        this.attackSpeed = attackSpeed;
        this.life = life;
        this.choiceShot = choiceShot;
        this.score = score;
    }

    public Integer getMonete() {
        return monete;
    }

    public void setMonete(Integer monete) {
        this.monete = monete;
    }

    public Integer getCollezionabili() {
        return collezionabili;
    }

    public void setCollezionabili(Integer collezionabili) {
        this.collezionabili = collezionabili;
    }

    public String getCheckpoint() {
        return checkpoint;
    }

    public void setCheckpoint(String checkpoint) {
        this.checkpoint = checkpoint;
    }

    public Double getSpeed() {
        return speed;
    }

    public void setSpeed(Double speed) {
        this.speed = speed;
    }

    public Integer getAttackDamage() {
        return attackDamage;
    }

    public void setAttackDamage(Integer attackDamage) {
        this.attackDamage = attackDamage;
    }

    public Integer getShotDamage() {
        return shotDamage;
    }

    public void setShotDamage(Integer shotDamage) {
        this.shotDamage = shotDamage;
    }

    public Double getAttackSpeed() {
        return attackSpeed;
    }

    public void setAttackSpeed(Double attackSpeed) {
        this.attackSpeed = attackSpeed;
    }

    public Double getLife() {
        return life;
    }

    public void setLife(Double life) {
        this.life = life;
    }

    public Integer getChoiceShot() {
        return choiceShot;
    }

    public void setChoiceShot(Integer choiceShot) {
        this.choiceShot = choiceShot;
    }

    public Integer getScore() {
        return score;
    }

    public void setScore(Integer score) {
        this.score = score;
    }

}
