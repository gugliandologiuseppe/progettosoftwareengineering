package com.se.se_server.beans;

import java.sql.Timestamp;

/**
 *
 * @author Gugliandolo & Infortuna
 */
public class RegistrationInput {
    private String nome;
    private String cognome;
    private String username;
    private Timestamp dataNascita;
    private String sesso;
    private String email;
    private String password;

    private RegistrationInput() {
    }

    public RegistrationInput(String nome, String cognome, String username, Timestamp dataNascita, String sesso, String email, String password) {
        this.nome = nome;
        this.cognome = cognome;
        this.username = username;
        this.dataNascita = dataNascita;
        this.sesso = sesso;
        this.email = email;
        this.password = password;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getCognome() {
        return cognome;
    }

    public void setCognome(String cognome) {
        this.cognome = cognome;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public Timestamp getDataNascita() {
        return dataNascita;
    }

    public void setDataNascita(Timestamp dataNascita) {
        this.dataNascita = dataNascita;
    }

    public String getSesso() {
        return sesso;
    }

    public void setSesso(String sesso) {
        this.sesso = sesso;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
        
}
