package com.se.se_server.beans;

/**
 *
 * @author Gugliandolo & Infortuna
 */
public class AggiornaPasswordResponse extends Response {
    private Boolean aggiornamentoEseguito;

    public AggiornaPasswordResponse() {
        super();
    }

    public AggiornaPasswordResponse(StatoLogin loginStatus, Boolean aggiornamentoEseguito) {
        super(loginStatus);
        this.aggiornamentoEseguito = aggiornamentoEseguito;
    }

    public boolean isAggiornamentoEseguito() {
        if(aggiornamentoEseguito == null)
            return false;

        return aggiornamentoEseguito;
    }

    public void setAggiornamentoEseguito(boolean aggiornamentoEseguito) {
        this.aggiornamentoEseguito = aggiornamentoEseguito;
    }
    
}
