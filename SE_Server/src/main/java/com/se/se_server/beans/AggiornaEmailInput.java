package com.se.se_server.beans;

/**
 *
 * @author Gugliandolo & Infortuna
 */
public class AggiornaEmailInput extends Input {
    private String email;

    public AggiornaEmailInput() {
        super();
    }

    public AggiornaEmailInput(long idSessione, String email) {
        super(idSessione);
        this.email = email;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
    
}
