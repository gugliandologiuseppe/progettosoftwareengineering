package com.se.se_server.beans;

/**
 *
 * @author Gugliandolo & Infortuna
 */
public class UpdateCheckpointInput extends Input {
    private CheckpointData dati;

    public UpdateCheckpointInput() {
        super();
    }

    public UpdateCheckpointInput(long idSessione, CheckpointData dati) {
        super(idSessione);
        this.dati = dati;
    }

    public CheckpointData getDati() {
        return dati;
    }

    public void setDati(CheckpointData dati) {
        this.dati = dati;
    }

}
