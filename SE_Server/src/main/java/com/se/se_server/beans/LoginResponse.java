package com.se.se_server.beans;

/**
 *
 * @author Gugliandolo & Infortuna
 */
public class LoginResponse extends Response {
    private Long idSessione;
    private String nomeUtente;
    private CheckpointData checkpointData;
    
    public LoginResponse() {
        super();
    }

    public LoginResponse(StatoLogin statoLogin) {
        super(statoLogin);
    }

    public LoginResponse(StatoLogin statoLogin, Long idSessione, String nomeUtente, CheckpointData checkpointData) {
        super(statoLogin);
        this.idSessione = idSessione;
        this.nomeUtente = nomeUtente;
        this.checkpointData = checkpointData;
    }

    public Long getIdSessione() {
        return idSessione;
    }

    public void setIdSessione(Long idSessione) {
        this.idSessione = idSessione;
    }

    public String getNomeUtente() {
        return nomeUtente;
    }

    public void setNomeUtente(String nomeUtente) {
        this.nomeUtente = nomeUtente;
    }

    public CheckpointData getCheckpointData() {
        return checkpointData;
    }

    public void setCheckpointData(CheckpointData checkpointData) {
        this.checkpointData = checkpointData;
    }

}
