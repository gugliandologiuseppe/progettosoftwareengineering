package com.se.se_server.beans;

import java.util.List;

/**
 *
 * @author Gugliandolo & Infortuna
 */
public class GetBestScoresResponse extends Response {
    private List<ScoreData> bestScores;

    public GetBestScoresResponse() {
        super();
    }

    public GetBestScoresResponse(StatoLogin statoLogin) {
        super(statoLogin);
    }
    
    public GetBestScoresResponse(StatoLogin statoLogin, List<ScoreData> bestScores) {
        super(statoLogin);
        this.bestScores = bestScores;
    }

    public List<ScoreData> getBestScores() {
        return bestScores;
    }

    public void setBestScores(List<ScoreData> bestScores) {
        this.bestScores = bestScores;
    }
    
}
