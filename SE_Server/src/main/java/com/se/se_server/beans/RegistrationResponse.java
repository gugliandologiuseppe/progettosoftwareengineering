package com.se.se_server.beans;

/**
 *
 * @author Gugliandolo & Infortuna
 */
public class RegistrationResponse {
    private String response;

    private RegistrationResponse() {
    }

    public RegistrationResponse(String response) {
        this.response = response;
    }

    public String getResponse() {
        return response;
    }

    public void setResponse(String response) {
        this.response = response;
    }
    
}
