package com.se.se_server.beans;

/**
 *
 * @author Gugliandolo & Infortuna
 */
public abstract class Input {
    private long idSessione;

    public Input() {
    }

    public Input(long idSessione) {
        this.idSessione = idSessione;
    }

    public final long getIdSessione() {
        return idSessione;
    }

    public final void setIdSessione(long idSessione) {
        this.idSessione = idSessione;
    }
    
}
