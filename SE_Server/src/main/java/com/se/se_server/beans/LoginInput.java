package com.se.se_server.beans;

/**
 *
 * @author Gugliandolo & Infortuna
 */
public class LoginInput {
    private String email;
    private String password;

    private LoginInput() {
    }

    public LoginInput(String email, String password) {
        this.email = email;
        this.password = password;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
    
}
