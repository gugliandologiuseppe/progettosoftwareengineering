package com.se.se_server.beans;

/**
 *
 * @author Gugliandolo & Infortuna
 */
public class ScoreData {
    private String username;
    private Integer collezionabili; 
    private Integer score; 
    
    public ScoreData() {
    }

    public ScoreData(String username, Integer collezionabili, Integer score) {
        this.username = username;
        this.collezionabili = collezionabili;
        this.score = score;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public Integer getCollezionabili() {
        return collezionabili;
    }

    public void setCollezionabili(Integer collezionabili) {
        this.collezionabili = collezionabili;
    }

    public Integer getScore() {
        return score;
    }

    public void setScore(Integer score) {
        this.score = score;
    }

    
}
