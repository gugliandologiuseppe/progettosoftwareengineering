package com.se.se_server.utility;

import java.security.SecureRandom;
import java.util.Objects;
import java.util.Random;

/**
 *
 * @author Gugliandolo & Infortuna
 */
public class RandomString {

    public static final String UPPER = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";

    public static final String LOWER = UPPER.toLowerCase();

    public static final String DIGITS = "0123456789";

    public static final String ALPHANUM = UPPER + LOWER + DIGITS;

    public static String getRandomString(int length, Random random, String symbols) {
        if (length < 1) throw new IllegalArgumentException();
        if (symbols.length() < 2) throw new IllegalArgumentException();
        Random rand = Objects.requireNonNull(random);
        char [] buf = new char[length];
        for (int idx = 0; idx < buf.length; ++idx)
            buf[idx] = symbols.charAt(rand.nextInt(symbols.length()));
        return new String(buf);
    }

    /**
     * Create an alphanumeric string generator.
     */
    public static String getRandomString(int length, Random random) {
        return getRandomString(length, random, ALPHANUM);
    }

    /**
     * Create an alphanumeric strings from a secure generator.
     */
    public static String getRandomString(int length) {
        return getRandomString(length, new SecureRandom());
    }

}
