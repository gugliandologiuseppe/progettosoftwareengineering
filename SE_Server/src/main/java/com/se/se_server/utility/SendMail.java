package com.se.se_server.utility;

import java.util.*;
import javax.mail.*;
import javax.mail.internet.*;

/**
 *
 * @author Gugliandolo & Infortuna
 */
public class SendMail {

    private static final String USERNAME = "seprojectggfi@gmail.com";
    private static final String PASSWORD = "gfseproject"; 
    
    public static void send(String subject, String body, String recipient) throws MessagingException {
        String[] to = { recipient }; // list of recipient email addresses
        sendFromGMail(to, subject, body);
    }

    private static void sendFromGMail(String[] to, String subject, String body) throws MessagingException {
        Properties props = System.getProperties();
        String host = "smtp.gmail.com";
        props.put("mail.smtp.starttls.enable", "true");
        props.put("mail.smtp.host", host);
        props.put("mail.smtp.user", USERNAME);
        props.put("mail.smtp.password", PASSWORD);
        props.put("mail.smtp.port", "25");
        props.put("mail.smtp.auth", "true");

        Session session = Session.getDefaultInstance(props);
        MimeMessage message = new MimeMessage(session);

        message.setFrom(new InternetAddress(USERNAME));
        InternetAddress[] toAddress = new InternetAddress[to.length];

        // To get the array of addresses
        for( int i = 0; i < to.length; i++ ) {
            toAddress[i] = new InternetAddress(to[i]);
        }

        for( int i = 0; i < toAddress.length; i++) {
            message.addRecipient(Message.RecipientType.TO, toAddress[i]);
        }

        message.setSubject(subject);
        message.setText(body);
        Transport transport = session.getTransport("smtp");
        transport.connect(host, USERNAME, PASSWORD);
        transport.sendMessage(message, message.getAllRecipients());
        transport.close();
    }
    
}