package com.se.se_server.repositories;

import com.se.se_server.database.StatoSessione;
import org.springframework.data.repository.CrudRepository;

/**
 *
 * @author Gugliandolo & Infortuna
 */
public interface RepositoryStatoSessione extends CrudRepository<StatoSessione, Long>{
    
}
