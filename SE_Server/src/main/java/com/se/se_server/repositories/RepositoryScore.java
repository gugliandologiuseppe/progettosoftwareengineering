package com.se.se_server.repositories;

import com.se.se_server.database.Score;
import java.util.List;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

/**
 *
 * @author Gugliandolo & Infortuna
 */
@Repository
public interface RepositoryScore extends CrudRepository<Score, String>{
    @Query("SELECT s FROM Score s WHERE s.username = ?1")
    List<Score> getScoreByUsername(String username);
    
    @Query("SELECT s FROM Score s WHERE s.username IS NOT NULL ORDER BY score DESC")
    List<Score> getBestScores();
}
