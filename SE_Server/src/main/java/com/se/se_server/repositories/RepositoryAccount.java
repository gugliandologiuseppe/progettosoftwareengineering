package com.se.se_server.repositories;

import com.se.se_server.database.Account;
import java.util.List;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

/**
 *
 * @author Gugliandolo & Infortuna
 */
@Repository
public interface RepositoryAccount extends CrudRepository<Account, String>{
    @Query("SELECT a FROM Account a WHERE a.username = ?1")
    List<Account> getAccountByUsername(String username);
}
