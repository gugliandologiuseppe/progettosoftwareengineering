package com.se.se_server.repositories;

import com.se.se_server.database.Account;
import com.se.se_server.database.Sessione;
import com.se.se_server.database.StatoSessione;
import java.sql.Timestamp;
import java.util.List;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author Gugliandolo & Infortuna
 */
public interface RepositorySessione extends CrudRepository<Sessione, Long>{
    @Query("SELECT s FROM Sessione s WHERE s.statoSessione = ?1 AND s.timestampUltimoMovimento < ?2")
    List<Sessione> getOpenSessions(StatoSessione statoSessione, Timestamp limiteSessioneAperta);
    
    @Query("SELECT s FROM Sessione s WHERE s.account = ?1 AND s.statoSessione = ?2")
    List<Sessione> getOpenSessions(Account account, StatoSessione statoSessione);
    
    @Query("SELECT s FROM Sessione s WHERE s.account = ?1 AND s.id <> ?2")
    List<Sessione> getAllSessions(Account oldAccount, long sessionIdNot4Update);
    
    @Query("SELECT s FROM Sessione s WHERE s.id = (SELECT MAX(s1.id) FROM Sessione s1 WHERE s.account = ?1)")
    List<Sessione> getLastSession(Account account);
}
