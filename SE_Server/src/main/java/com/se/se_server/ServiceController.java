package com.se.se_server;

import com.se.se_server.beans.AggiornaEmailInput;
import com.se.se_server.beans.AggiornaEmailResponse;
import com.se.se_server.beans.AggiornaPasswordInput;
import com.se.se_server.beans.AggiornaPasswordResponse;
import com.se.se_server.beans.GetBestScoresInput;
import com.se.se_server.beans.GetBestScoresResponse;
import com.se.se_server.beans.LoginInput;
import com.se.se_server.beans.LoginResponse;
import com.se.se_server.beans.LogoutInput;
import com.se.se_server.beans.LogoutResponse;
import com.se.se_server.beans.RegistrationInput;
import com.se.se_server.beans.RegistrationResponse;
import com.se.se_server.beans.UpdateCheckpointInput;
import com.se.se_server.beans.UpdateCheckpointReponse;
import com.se.se_server.service.ServiziImpl;
import java.io.IOException;
import java.security.NoSuchAlgorithmException;
import java.util.Timer;
import java.util.TimerTask;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 *
 * @author Gugliandolo & Infortuna
 */
@Controller
@SpringBootApplication
@EnableJpaRepositories
public class ServiceController {
    //private final Timer timer;
    //private final int LIMITE_SESSIONE_APERTA = 1000*60*5; //5 minuti [1000 millis = 1 secondo * 60 = 1 minuto * 5 = 5 minuti]

    @Autowired
    private ServiziImpl servizi;
    

    /*public ServiceController() {
        timer = new Timer();
        timer.schedule(new TimerTask() {
            @Override
            public void run() {
                servizi.chiudiSessioniAperte(LIMITE_SESSIONE_APERTA);
            }
        }, LIMITE_SESSIONE_APERTA);
    }*/
    
    
    @PostMapping("/RegistrationService")
    @ResponseBody
    public RegistrationResponse registrationService(@RequestBody RegistrationInput payload) {
        try {
            return servizi.doRegistration(payload);
        } catch(IOException | NoSuchAlgorithmException ex) {
            return new RegistrationResponse("Errore: " + ex.getMessage());
        }
    }
    
    @PostMapping("/LoginService")
    @ResponseBody
    public LoginResponse loginService(@RequestBody LoginInput input) throws NoSuchAlgorithmException, IOException {
        return servizi.doLogin(input);
    }
    
    @PostMapping("/LogoutService")
    @ResponseBody
    public LogoutResponse loginService(@RequestBody LogoutInput input) throws IOException {
        return servizi.doLogout(input);
    }
    
    @PostMapping("/EmailUpdate")
    @ResponseBody
    public AggiornaEmailResponse emailUpdate(@RequestBody AggiornaEmailInput input) throws IOException {
        return servizi.aggiornaEmail(input);
    }
    
    @PostMapping("/PasswordUpdate")
    @ResponseBody
    public AggiornaPasswordResponse passwordUpdate(@RequestBody AggiornaPasswordInput input) throws IOException {
        return servizi.aggiornaPassword(input);
    }
    
    @PostMapping("/UpdateCheckpoint")
    @ResponseBody
    public UpdateCheckpointReponse checkpointUpdate(@RequestBody UpdateCheckpointInput input) throws IOException {
        return servizi.aggiornaCheckpoint(input);
    }
    
    @PostMapping("/getScores")
    @ResponseBody
    public GetBestScoresResponse getScores(@RequestBody GetBestScoresInput input) throws IOException {
        return servizi.getBestScores(input);
    }
    
}
