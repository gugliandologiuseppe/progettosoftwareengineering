package com.se.se_server.database;

import java.io.Serializable;
import java.sql.Timestamp;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

/**
 *
 * @author Gugliandolo & Infortuna
 */
@Entity
public class Sessione implements Serializable { 
    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;
    
    @ManyToOne
    @JoinColumn(name = "email")
    private Account account;

    @Column(name = "timestamp_apertura")
    private Timestamp timestampApertura;

    @Column(name = "timestamp_chiusura")
    private Timestamp timestampChiusura;

    @ManyToOne
    @JoinColumn(name = "stato_sessione")
    private StatoSessione statoSessione;
  
    @Column(name = "timestamp_ultimo_movimento")
    private Timestamp timestampUltimoMovimento;

    @Column(name = "monete")
    private Integer monete;
    
    @Column(name = "collezionabili")
    private Integer collezionabili;

    @Column(name = "mondo_checkpoint")
    private String checkpoint;
    
    @Column(name = "speed")
    private Double speed;
    
    @Column(name = "attack_damage")
    private Integer attackDamage;
    
    @Column(name = "shot_damage")
    private Integer shotDamage;
    
    @Column(name = "attack_speed")
    private Double attackSpeed;
    
    @Column(name = "life")
    private Double life;
    
    @Column(name = "choice_shot")
    private Integer choiceShot;
    
    @Column(name = "score")
    private Integer score;
    
    protected Sessione() {
    }

    public Sessione(Long id, Account account, Timestamp timestampApertura, Timestamp timestampChiusura, StatoSessione statoSessione, Timestamp timestampUltimoMovimento, Integer monete, Integer collezionabili, String checkpoint) {
        this.id = id;
        this.account = account;
        this.timestampApertura = timestampApertura;
        this.timestampChiusura = timestampChiusura;
        this.statoSessione = statoSessione;
        this.timestampUltimoMovimento = timestampUltimoMovimento;
        this.monete = monete;
        this.collezionabili = collezionabili;
        this.checkpoint = checkpoint;
    }

    public Sessione(Long id, Account account, Timestamp timestampApertura, Timestamp timestampChiusura, StatoSessione statoSessione, Timestamp timestampUltimoMovimento, Integer monete, Integer collezionabili, String checkpoint, Double speed, Integer attackDamage, Integer shotDamage, Double attackSpeed, Double life, Integer choiceShot, Integer score) {
        this.id = id;
        this.account = account;
        this.timestampApertura = timestampApertura;
        this.timestampChiusura = timestampChiusura;
        this.statoSessione = statoSessione;
        this.timestampUltimoMovimento = timestampUltimoMovimento;
        this.monete = monete;
        this.collezionabili = collezionabili;
        this.checkpoint = checkpoint;
        this.speed = speed;
        this.attackDamage = attackDamage;
        this.shotDamage = shotDamage;
        this.attackSpeed = attackSpeed;
        this.life = life;
        this.choiceShot = choiceShot;
        this.score = score;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Account getAccount() {
        return account;
    }

    public void setAccount(Account account) {
        this.account = account;
    }

    public Timestamp getTimestampApertura() {
        return timestampApertura;
    }

    public void setTimestampApertura(Timestamp timestampApertura) {
        this.timestampApertura = timestampApertura;
    }

    public Timestamp getTimestampChiusura() {
        return timestampChiusura;
    }

    public void setTimestampChiusura(Timestamp timestampChiusura) {
        this.timestampChiusura = timestampChiusura;
    }

    public StatoSessione getStatoSessione() {
        return statoSessione;
    }

    public void setStatoSessione(StatoSessione statoSessione) {
        this.statoSessione = statoSessione;
    }

    public Timestamp getTimestampUltimoMovimento() {
        return timestampUltimoMovimento;
    }

    public void setTimestampUltimoMovimento(Timestamp timestampUltimoMovimento) {
        this.timestampUltimoMovimento = timestampUltimoMovimento;
    }

    public Integer getMonete() {
        return monete;
    }

    public void setMonete(Integer monete) {
        this.monete = monete;
    }

    public Integer getCollezionabili() {
        return collezionabili;
    }

    public void setCollezionabili(Integer collezionabili) {
        this.collezionabili = collezionabili;
    }

    public String getCheckpoint() {
        return checkpoint;
    }

    public void setCheckpoint(String checkpoint) {
        this.checkpoint = checkpoint;
    }

    public Double getSpeed() {
        return speed;
    }

    public void setSpeed(Double speed) {
        this.speed = speed;
    }

    public Integer getAttackDamage() {
        return attackDamage;
    }

    public void setAttackDamage(Integer attackDamage) {
        this.attackDamage = attackDamage;
    }

    public Integer getShotDamage() {
        return shotDamage;
    }

    public void setShotDamage(Integer shotDamage) {
        this.shotDamage = shotDamage;
    }

    public Double getAttackSpeed() {
        return attackSpeed;
    }

    public void setAttackSpeed(Double attackSpeed) {
        this.attackSpeed = attackSpeed;
    }

    public Double getLife() {
        return life;
    }

    public void setLife(Double life) {
        this.life = life;
    }

    public Integer getChoiceShot() {
        return choiceShot;
    }

    public void setChoiceShot(Integer choiceShot) {
        this.choiceShot = choiceShot;
    }

    public Integer getScore() {
        return score;
    }

    public void setScore(Integer score) {
        this.score = score;
    }
    
}

