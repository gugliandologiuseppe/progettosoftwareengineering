package com.se.se_server.database;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

/**
 *
 * @author Gugliandolo & Infortuna
 */
@Entity
public class StatoSessione implements Serializable { 
    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    @Column(name = "id")
    private long id;  
    
    @Column(name = "descrizione")
    private String descrizione;

    protected StatoSessione() {
    } 
    
    public StatoSessione(long id, String descrizione) {
        this.id = id;
        this.descrizione = descrizione;
    }
    
    public long getId() {
        return id;
    }

    public String getDescrizione() {
        return descrizione;
    }
   
}
