package com.se.se_server.database;

import java.io.Serializable;
import java.sql.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

/**
 *
 * @author Gugliandolo & Infortuna
 */
@Entity
public class Score implements Serializable { 
    @Id
    @Column(name = "username")
    private String username;
    
    @Column(name = "score")
    private Integer score;
    
    @Column(name = "collezionabili")
    private Integer collezionabili;

    public Score() {
    }

    public Score(String username, Integer score, Integer collezionabili) {
        this.username = username;
        this.score = score;
        this.collezionabili = collezionabili;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public Integer getScore() {
        return score;
    }

    public void setScore(Integer score) {
        this.score = score;
    }

    public Integer getCollezionabili() {
        return collezionabili;
    }

    public void setCollezionabili(Integer collezionabili) {
        this.collezionabili = collezionabili;
    }
    
}

