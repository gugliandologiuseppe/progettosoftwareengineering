package com.se.se_server.database;

import java.io.Serializable;
import java.sql.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

/**
 *
 * @author Gugliandolo & Infortuna
 */
@Entity
public class Account implements Serializable { 
    @Id
    @Column(name = "email")
    private String email;
    
    @Column(name = "nome")
    private String nome;
    
    @Column(name = "cognome")
    private String cognome;
    
    @Column(name = "username")
    private String username;
    
    @Column(name = "sesso")
    private String sesso;
    
    @Column(name = "data_nascita")
    private Date dataNascita;
    
    @Column(name = "password")
    private String password;

    @Column(name = "sale")
    private String sale;

    @Column(name = "data_registrazione")
    private Date dataRegistrazione;

    @Column(name = "attivo")
    private short attivo;
  
    @Column(name = "scadenza_password")
    private Date scadenzaPassword;
    
    @Column(name = "password_errata")
    private short passwordErrata;
    
    @Column(name = "codice_authenticator")
    private String codiceAuthenticator;

    public Account() {
    }

    public Account(
            String email, String nome, String cognome, String username,
            String sesso, Date dataNascita, String password, String sale,
            Date dataRegistrazione, short attivo, Date scadenzaPassword,
            short passwordErrata, String codiceAuthenticator
    ) {
        this.email = email;
        this.nome = nome;
        this.cognome = cognome;
        this.username = username;
        this.sesso = sesso;
        this.dataNascita = dataNascita;
        this.password = password;
        this.sale = sale;
        this.dataRegistrazione = dataRegistrazione;
        this.attivo = attivo;
        this.scadenzaPassword = scadenzaPassword;
        this.passwordErrata = passwordErrata;
        this.codiceAuthenticator = codiceAuthenticator;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getCognome() {
        return cognome;
    }

    public void setCognome(String cognome) {
        this.cognome = cognome;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getSesso() {
        return sesso;
    }

    public void setSesso(String sesso) {
        this.sesso = sesso;
    }

    public Date getDataNascita() {
        return dataNascita;
    }

    public void setDataNascita(Date dataNascita) {
        this.dataNascita = dataNascita;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getSale() {
        return sale;
    }

    public void setSale(String sale) {
        this.sale = sale;
    }

    public Date getDataRegistrazione() {
        return dataRegistrazione;
    }

    public void setDataRegistrazione(Date dataRegistrazione) {
        this.dataRegistrazione = dataRegistrazione;
    }

    public short getAttivo() {
        return attivo;
    }

    public void setAttivo(short attivo) {
        this.attivo = attivo;
    }
    
    public boolean isAttivo() {
        return attivo == 1;
    }
    
    public void setAttivo(boolean attivo) {
        this.attivo = (short) (attivo ? 1 : 0);
    }

    public Date getScadenzaPassword() {
        return scadenzaPassword;
    }

    public void setScadenzaPassword(Date scadenzaPassword) {
        this.scadenzaPassword = scadenzaPassword;
    }

    public short getPasswordErrata() {
        return passwordErrata;
    }

    public void setPasswordErrata(short passwordErrata) {
        this.passwordErrata = passwordErrata;
    }

    public String getCodiceAuthenticator() {
        return codiceAuthenticator;
    }

    public void setCodiceAuthenticator(String codiceAuthenticator) {
        this.codiceAuthenticator = codiceAuthenticator;
    }

}

