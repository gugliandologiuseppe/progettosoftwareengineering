package com.se.se_server.service;

import com.se.se_server.beans.AggiornaEmailInput;
import com.se.se_server.beans.AggiornaEmailResponse;
import com.se.se_server.beans.AggiornaPasswordInput;
import com.se.se_server.beans.AggiornaPasswordResponse;
import com.se.se_server.beans.CheckpointData;
import com.se.se_server.beans.GetBestScoresInput;
import com.se.se_server.beans.GetBestScoresResponse;
import com.se.se_server.beans.Response;
import com.se.se_server.beans.LoginInput;
import com.se.se_server.beans.LoginResponse;
import com.se.se_server.beans.LogoutInput;
import com.se.se_server.beans.LogoutResponse;
import com.se.se_server.beans.RegistrationInput;
import com.se.se_server.beans.RegistrationResponse;
import com.se.se_server.beans.ScoreData;
import com.se.se_server.beans.UpdateCheckpointInput;
import com.se.se_server.beans.UpdateCheckpointReponse;
import com.se.se_server.database.Account;
import com.se.se_server.database.Score;
import com.se.se_server.database.Sessione;
import com.se.se_server.utility.Encrypt;
import com.se.se_server.utility.RandomString;
import com.se.se_server.utility.SendMail;
import java.io.IOException;
import java.security.NoSuchAlgorithmException;
import java.sql.Date;
import java.util.Calendar;
import java.util.GregorianCalendar;
import javax.persistence.EntityManagerFactory;
import javax.persistence.PersistenceUnit;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import com.se.se_server.repositories.RepositoryAccount;
import com.se.se_server.repositories.RepositoryScore;
import com.se.se_server.repositories.RepositorySessione;
import com.se.se_server.repositories.RepositoryStatoSessione;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import javassist.NotFoundException;
import javax.security.auth.login.LoginException;
import org.springframework.beans.BeanUtils;


/**
 *
 * @author Gugliandolo & Infortuna
 */
@Service
public class ServiziImpl {
       
    @Autowired
    private RepositoryAccount accountRepository;
    
    @Autowired
    private RepositorySessione repositorySessione;
    
    @Autowired
    private RepositoryStatoSessione repositoryStatoSessione;
    
    @Autowired
    private RepositoryScore repositoryScore;
    
    @PersistenceUnit
    private EntityManagerFactory emf;

    protected SessionFactory getSessionFactory() {
      return emf.unwrap(SessionFactory.class);
    }
    
    @Transactional
    public RegistrationResponse doRegistration(RegistrationInput input) throws NoSuchAlgorithmException, IOException {
        try {
            List<Account> accountPerUsername = accountRepository.getAccountByUsername(input.getUsername());
            if(accountPerUsername.size() > 0) {
                return new RegistrationResponse("Username già utilizzato, indicarne un altro");
            }
            
            final String sale = RandomString.getRandomString(100);
            
            final Account account = new Account();
            account.setNome(input.getNome());
            account.setCognome(input.getCognome());
            account.setUsername(input.getUsername());
            account.setSesso(input.getSesso());
            account.setDataNascita(new Date(input.getDataNascita().getTime()));
            account.setEmail(input.getEmail());
            account.setPassword(Encrypt.generateSHA512(input.getPassword(), sale));
            account.setSale(sale);
            account.setAttivo(true);
            account.setDataRegistrazione(new Date(System.currentTimeMillis()));
            account.setPasswordErrata((short) 0);
            GregorianCalendar c = new GregorianCalendar();
            c.setTime(new java.util.Date(System.currentTimeMillis()));
            c.add(Calendar.MONTH, 6);
            account.setScadenzaPassword(new Date(c.getTimeInMillis()));
            
            accountRepository.save(account);
            
            final Score score = new Score(account.getUsername(), 0, 0);
            repositoryScore.save(score);
        } finally {
            
        }
        
        try {
            String subject = "Registrazione effettuata con successo";
            String body = "Benvenuto " + input.getNome() + ",\n\n\n"
                    + "la registrazione al gioco e' avvenuta con successo.\n"
                    + "Ti diamo il benvenuto e speriamo il servizio ti sia utile.\n"
                    + "\nGrazie e buon divertimento,\nil Team.";

            SendMail.send(subject, body, input.getEmail());
        } catch (Exception ex) {
            return new RegistrationResponse("Registrazione avvenuta con successo"); //eccezione durante l'invio della mail";
        }
        
        return new RegistrationResponse("Registrazione avvenuta con successo, è stata inviata una mail di conferma");
    }

    @Transactional
    public LoginResponse doLogin(LoginInput input) throws NoSuchAlgorithmException, IOException {
        final Optional<Account> ricercaAccount = accountRepository.findById(input.getEmail());
        if(!ricercaAccount.isPresent()) {
            return new LoginResponse(LoginResponse.StatoLogin.CREDENZIALI_ERRATE);
        }
        
        final Account account = ricercaAccount.get();
        if(account.getPasswordErrata() == 5) {
            return new LoginResponse(LoginResponse.StatoLogin.LIMITE_ERRORI_PASS);
        }
        
        final String shaPassword = Encrypt.generateSHA512(input.getPassword(), account.getSale());
        if(!shaPassword.equals(account.getPassword())) {
            account.setPasswordErrata((short)(account.getPasswordErrata() +  1));
            return new LoginResponse(LoginResponse.StatoLogin.CREDENZIALI_ERRATE);
        }
        
        final Timestamp currentTimestamp = getCurrentTimestamp();
        
        final LoginResponse.StatoLogin statoLogin =
                (account.getScadenzaPassword().before(new Date(currentTimestamp.getTime())) ?
                         Response.StatoLogin.PASSWORD_SCADUTA : LoginResponse.StatoLogin.LOGGATO);
        
        CheckpointData checkpoint = null;
        final List<Sessione> sessioniAperte = getOpenSessions(account);
        for(Sessione sessioneAperta : sessioniAperte) {
            sessioneAperta.setStatoSessione(repositoryStatoSessione.findById(LoginResponse.StatoLogin.SLOGGATO.value).get());
            sessioneAperta.setTimestampChiusura(currentTimestamp);
            repositorySessione.save(sessioneAperta);
            if(sessioniAperte.size() == 1) {
                checkpoint = new CheckpointData(sessioneAperta.getMonete(), sessioneAperta.getCollezionabili(), sessioneAperta.getCheckpoint(), sessioneAperta.getSpeed(), sessioneAperta.getAttackDamage(), sessioneAperta.getShotDamage(), sessioneAperta.getAttackSpeed(), sessioneAperta.getLife(), sessioneAperta.getChoiceShot(), sessioneAperta.getScore());
            }
        }
        
        if(checkpoint == null) {
            List<Sessione> sessioni = repositorySessione.getLastSession(account);
            if(sessioni.size() > 0) {
                Sessione last = repositorySessione.getLastSession(account).get(0);
                checkpoint = new CheckpointData(last.getMonete(), last.getCollezionabili(), last.getCheckpoint(), last.getSpeed(), last.getAttackDamage(), last.getShotDamage(), last.getAttackSpeed(), last.getLife(), last.getChoiceShot(), last.getScore());
            }
        }
        
        account.setPasswordErrata((short) 0);
        accountRepository.save(account);
        
        final Sessione sessione = new Sessione(null, account, currentTimestamp, null, repositoryStatoSessione.findById(statoLogin.value).get(), currentTimestamp, null, null, null);
        
        repositorySessione.save(sessione);
                
        return new LoginResponse(statoLogin, sessione.getId(), account.getUsername(), checkpoint);
    }
    
    public LogoutResponse doLogout(LogoutInput input) throws IOException {
        Optional<Sessione> result = repositorySessione.findById(input.getIdSessione());
        if(!result.isPresent())
            return new LogoutResponse("Sessione non trovata");
        
        final Timestamp currentTimestamp = getCurrentTimestamp();
        
        final Sessione s = result.get();
        s.setStatoSessione(repositoryStatoSessione.findById(LoginResponse.StatoLogin.SLOGGATO.value).get());
        s.setTimestampChiusura(currentTimestamp);
        s.setTimestampUltimoMovimento(currentTimestamp);
        repositorySessione.save(s);
                
        return new LogoutResponse("Logout eseguito con successo");
    }

    public AggiornaEmailResponse aggiornaEmail(AggiornaEmailInput input) throws IOException {
        final Sessione sessione;
        try {
            sessione = checkStatoSessione(input.getIdSessione());
        } catch (NotFoundException | LoginException ex) {
            return new AggiornaEmailResponse(Response.StatoLogin.SLOGGATO);
        }
        
        final Optional<Account> optionalAccount = accountRepository.findById(input.getEmail());
        if(optionalAccount.isPresent()) {
            return new AggiornaEmailResponse(Response.StatoLogin.LOGGATO, false);
        }
        
        Account account = new Account();
        Account oldAccount = sessione.getAccount();
        //Copia tutti i campi con lo stesso nome tra due classi
        BeanUtils.copyProperties(oldAccount, account);
        account.setEmail(input.getEmail());
        
        final String username = account.getUsername();
        account.setUsername("--");
        accountRepository.save(account);
        
        List<Sessione> sessioni = repositorySessione.getAllSessions(oldAccount, sessione.getId());
        for(Sessione s : sessioni) {
            s.setAccount(account);
            repositorySessione.save(s);
        }
        
        sessione.setAccount(account);
        sessione.setTimestampUltimoMovimento(getCurrentTimestamp());
        
        repositorySessione.save(sessione);
        accountRepository.delete(oldAccount);
        
        account.setUsername(username);
        accountRepository.save(account);
        
        return new AggiornaEmailResponse(Response.StatoLogin.LOGGATO, true);
        
    }
    
    public AggiornaPasswordResponse aggiornaPassword(AggiornaPasswordInput input) throws IOException {
        final Optional<Sessione> optional = repositorySessione.findById(input.getIdSessione());
        if(!optional.isPresent()) {
            return new AggiornaPasswordResponse(Response.StatoLogin.SLOGGATO, null);
        }
        
        final Sessione sessione = optional.get();
        final boolean passwordScaduta = sessione.getStatoSessione().getId() != Response.StatoLogin.PASSWORD_SCADUTA.value;
        if(!passwordScaduta) {
            try {
                checkStatoSessione(sessione);
            } catch (LoginException ex) {
                return new AggiornaPasswordResponse(Response.StatoLogin.SLOGGATO, null);
            }
        }
        
        final Account account = sessione.getAccount();

        try {
            final String oldPassword = Encrypt.generateSHA512(input.getOldPassword(), account.getSale());
            if(!oldPassword.equalsIgnoreCase(account.getPassword())) {
                return new AggiornaPasswordResponse(Response.StatoLogin.LOGGATO, false);
            }
            
            account.setPassword(Encrypt.generateSHA512(input.getPassword(), account.getSale()));
            accountRepository.save(account);
            
            if(passwordScaduta) {
                sessione.setStatoSessione(repositoryStatoSessione.findById(Response.StatoLogin.LOGGATO.value).get());
                repositorySessione.save(sessione);
            }
        } catch(NoSuchAlgorithmException ex) {
           throw new IOException(ex.toString());
        } 
        
        sessione.setTimestampUltimoMovimento(getCurrentTimestamp());
        repositorySessione.save(sessione);
        
        return new AggiornaPasswordResponse(Response.StatoLogin.LOGGATO, true);
    }

    public void chiudiSessioniAperte(int limiteSessioneAperta) {
        List<Sessione> sessioniAperte = getOpenSessions(new Timestamp(System.currentTimeMillis()-limiteSessioneAperta));
        for(Sessione s : sessioniAperte) {
            s.setStatoSessione(repositoryStatoSessione.findById(Response.StatoLogin.SESSIONE_SCADUTA.value).get());
            s.setTimestampChiusura(getCurrentTimestamp());
        }
        repositorySessione.saveAll(sessioniAperte);
    }
     
    private List<Sessione> getOpenSessions(Timestamp limiteSessioneAperta) {
        return repositorySessione.getOpenSessions(repositoryStatoSessione.findById(LoginResponse.StatoLogin.LOGGATO.value).get(), limiteSessioneAperta);
    }
    
    private List<Sessione> getOpenSessions(Account account) {
        return repositorySessione.getOpenSessions(account, repositoryStatoSessione.findById(LoginResponse.StatoLogin.LOGGATO.value).get());
    }
    
    private Sessione checkStatoSessione(long idSessione) throws NotFoundException, LoginException {
        final Optional<Sessione> optional = repositorySessione.findById(idSessione);
        if(!optional.isPresent()) {
            throw new NotFoundException("");
        }
        
        final Sessione sessione = optional.get();
        
        if(sessione.getStatoSessione().getId() == LoginResponse.StatoLogin.LOGGATO.value)
            return sessione;
                    
        throw new LoginException("Utente non loggato");
    }

    private void checkStatoSessione(Sessione sessione) throws LoginException {
        if(sessione.getStatoSessione().getId() == LoginResponse.StatoLogin.LOGGATO.value)
            return;

        throw new LoginException("Utente non loggato");
    }
    
    public UpdateCheckpointReponse aggiornaCheckpoint(UpdateCheckpointInput input) {
        final Sessione sessione;
        try {
            sessione = checkStatoSessione(input.getIdSessione());
        } catch (NotFoundException | LoginException ex) {
            return new UpdateCheckpointReponse(Response.StatoLogin.SLOGGATO);
        }
        
        CheckpointData data = input.getDati();
        
        sessione.setLife(data.getLife());
        sessione.setAttackDamage(data.getAttackDamage());
        sessione.setAttackSpeed(data.getAttackSpeed());
        sessione.setSpeed(data.getSpeed());
        sessione.setCollezionabili(data.getCollezionabili());
        sessione.setChoiceShot(data.getChoiceShot());
        sessione.setCheckpoint(data.getCheckpoint());
        sessione.setMonete(data.getMonete());
        sessione.setShotDamage(data.getShotDamage());
        sessione.setScore(data.getScore());
 
        System.out.println(sessione.getAttackDamage() == null ? "null" : sessione.getAttackDamage());
        System.out.println(sessione.getShotDamage() == null ? "null" : sessione.getShotDamage());
        System.out.println(sessione.getChoiceShot() == null ? "null" : sessione.getChoiceShot());
        
        repositorySessione.save(sessione);
        
        final Score score = repositoryScore.getScoreByUsername(sessione.getAccount().getUsername()).get(0);
        if(sessione.getScore() != null && sessione.getScore() > score.getScore()) {
            score.setCollezionabili(sessione.getCollezionabili());
            score.setScore(sessione.getScore());
            repositoryScore.save(score);
        }
        
        return new UpdateCheckpointReponse(Response.StatoLogin.LOGGATO, "SUCCES");
    }
    
    public  GetBestScoresResponse getBestScores(GetBestScoresInput input) {
        final Sessione sessione;
        try {
            sessione = checkStatoSessione(input.getIdSessione());
        } catch (NotFoundException | LoginException ex) {
            return new GetBestScoresResponse(Response.StatoLogin.SLOGGATO);
        }
        
        final Account account = sessione.getAccount();
        
        List<Score> bestScore = repositoryScore.getBestScores();
        
        boolean trovato = false;
        List<ScoreData> scoreForResponse = new ArrayList<>();
        for(int i = 0; i < 3; i++) {
            Score s = bestScore.get(i);
            trovato = trovato || account.getUsername().equals(s.getUsername());
            ScoreData data = new ScoreData();
            BeanUtils.copyProperties(s, data);
            scoreForResponse.add(data);
        }
        
        if(!trovato) {
            ScoreData data = new ScoreData();
            BeanUtils.copyProperties(repositoryScore.getScoreByUsername(account.getUsername()).get(0), data);
            scoreForResponse.add(data);
        }
        
        return new GetBestScoresResponse(Response.StatoLogin.LOGGATO, scoreForResponse);
    }
    
    final Timestamp getCurrentTimestamp() {
        return new Timestamp(System.currentTimeMillis());
    }
    
}
