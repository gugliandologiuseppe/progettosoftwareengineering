package com.se.se_server;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.context.annotation.PropertySource;

/**
 *
 * @author Gugliandolo & Infortuna
 */
@SpringBootApplication
@PropertySource("application.properties")
public class ServiceStarter extends SpringBootServletInitializer {
    
    public static void main(String[] args) {
        SpringApplication.run(ServiceController.class, args);
    }
    
}
