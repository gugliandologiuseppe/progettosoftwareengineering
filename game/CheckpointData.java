import java.io.Serializable;
import com.google.gson.annotations.Expose;

/**
 * Write a description of class CheckpointData here.
 * 
 * @author (your name) 
 * @version (a version number or a date)
 */
public class CheckpointData implements Serializable 
{
    @Expose
    public Integer monete;
    @Expose
    public Integer collezionabili;
    @Expose
    public String checkpoint;
    @Expose
    public Double speed;
    @Expose
    public Integer attackDamage;
    @Expose
    public Integer shotDamage;
    @Expose
    public Double attackSpeed;
    @Expose
    public Double life;
    @Expose
    public Integer choiceShot;
    @Expose
    public Integer score; 
    
    public CheckpointData() {
    }

    public CheckpointData(Integer monete, Integer collezionabili, String checkpoint, Double speed, Integer attackDamage, Integer shotDamage, Double attackSpeed, Double life, Integer choiceShot, Integer score) {
        this.monete = monete;
        this.collezionabili = collezionabili;
        this.checkpoint = checkpoint;
        this.speed = speed;
        this.attackDamage = attackDamage;
        this.shotDamage = shotDamage;
        this.attackSpeed = attackSpeed;
        this.life = life;
        this.choiceShot = choiceShot;
        this.score = score;
    }
}
