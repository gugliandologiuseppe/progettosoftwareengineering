import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)
import java.util.List;
/**
 * Write a description of class Mountain2 here.
 * 
 * @author (your name) 
 * @version (a version number or a date)
 */
public class Mountain2 extends SWorld
{
    private GreenfootImage bg;
    /**
     * Constructor for objects of class Mountain2.
     * 
     */
    public Mountain2()
    {    
        // Create a new world with 600x400 cells with a cell size of 1x1 pixels.
        super(850, 600, 1, 2000, 1300); 
        prepare();
        super.act(); 
    }
    /**
     * Prepare the world for the start of the program.
     * That is: create the initial objects and add them to the world.
     */
    private void prepare(){ 
        oggSfondo();
        bloccaStrada();
        
        setMainActor(new Pg(), 100, 400);
        mainActor.setLocation(-450, 450);    
        bg = new GreenfootImage("rock2.png");
        setScrollingBackground(bg);
        
    }
    public void act(){
        scrollObjects();
        scrollBackground();
        pause();
        nextWorld();
        stopWall();
        respawnFallPg();
    }
    /**
     * Il pg prende danno da caduta e ritorna ad inizio livello.
     */
    private void respawnFallPg(){
        List<Pg> PgList = getObjects(Pg.class);
        Pg pg = PgList.get(0);
        if(pg.fallVoid()){
            mainActor.setLocation(-500, 412);
        }
    }
    /**
     * Classe che porta al mondo successivo
    */  
    private void nextWorld(){
        List<Pg> PgList = getObjects(Pg.class);
        Pg pg = PgList.get(0);       
        if(pg.atRightEdge()){
            Greenfoot.setWorld(new Mountain3());
        }
    } 
    /**
     * si ferma alla parente tornando indietro. Questo viene fatto con la linea trasparente 'PerInterazioni'
     */
    private void stopWall(){
        List<Pg> PgList = getObjects(Pg.class);
        Pg pg = PgList.get(0);
        if(pg.PgBarra()>0 && Greenfoot.isKeyDown("right")){
            pg.setLocation(pg.getX()-30,pg.getY());
        }
        if(pg.PgBarra()>0 && Greenfoot.isKeyDown("left")){
            pg.setLocation(pg.getX()+30,pg.getY());
        }
    }
    /**
     * Oggetti creati per lo sfondo
     */
    private void oggSfondo(){
        BaseErba baseErba2 = new BaseErba(1);
        addObject(baseErba2,100,900);
        Terra terra5 = new Terra(2);
        terra5.getImage().scale(terra5.getImage().getWidth() +300, terra5.getImage().getHeight() +250);
        addObject(terra5,-410,800);
        Terra terra = new Terra(2);
        terra.getImage().scale(terra.getImage().getWidth() +100, terra.getImage().getHeight() +200);
        addObject(terra,1300,800);
        
        Alberi albero = new Alberi(4);
        albero.getImage().scale(albero.getImage().getWidth() -30, albero.getImage().getHeight() -35);
        addObject(albero,-300,290);
        Cespugli cespuglio = new Cespugli(6);
        addObject(cespuglio,-410,430);        
        BaseErba baseErba = new BaseErba(1);
        addObject(baseErba,250,320);
        BaseErba baseErba3 = new BaseErba(1);
        addObject(baseErba3,500,700);
        BaseErba baseErba4 = new BaseErba(1);
        addObject(baseErba4,800,450);
        BaseErba baseErba5 = new BaseErba(1);
        addObject(baseErba5,750,900);
        BaseErba baseErba6 = new BaseErba(1);
        addObject(baseErba6,1200,200);
        BaseErba baseErba7 = new BaseErba(1);
        addObject(baseErba7,750,0);
        BaseErba baseErba8 = new BaseErba(1);
        addObject(baseErba8,300,-200);
        BaseErba baseErba9 = new BaseErba(1);
        addObject(baseErba9,0,-200);
        BaseErba baseErba10 = new BaseErba(1);
        addObject(baseErba10,-550,-30);
        
        Cassa cassa = new Cassa();
        addObject(cassa,100,840);
        Cassa cassa1 = new Cassa();
        addObject(cassa1,0,-260);
        Cassa cassa2 = new Cassa();
        addObject(cassa2,140,-260);
        Quadrifoglio quadrifoglio = new Quadrifoglio();
        addObject(quadrifoglio,-485,-130);
        Cartelli cartello = new Cartelli(1);
        addObject(cartello,1300,455);
        
        Spikes spike = new Spikes(1);
        spike.turn(180);
        addObject(spike, 500,660);
    }
    private void bloccaStrada(){
        PerInterazioni linea = new PerInterazioni();
        linea.turn(90);
        addObject(linea,5,600);
        PerInterazioni linea2 = new PerInterazioni();
        linea2.turn(90);
        addObject(linea2,5,500);
        PerInterazioni linea3 = new PerInterazioni();
        linea3.turn(90);
        addObject(linea3,5,700);
        PerInterazioni linea4 = new PerInterazioni();
        linea4.turn(90);
        addObject(linea4,5,800);
        PerInterazioni linea5 = new PerInterazioni();
        linea5.turn(90);
        addObject(linea5,5,900);
        PerInterazioni linea6 = new PerInterazioni();
        linea6.turn(90);
        addObject(linea6,970,550);
        PerInterazioni linea7 = new PerInterazioni();
        linea7.turn(90);
        addObject(linea7,970,650);
        PerInterazioni linea8 = new PerInterazioni();
        linea8.turn(90);
        addObject(linea8,970,750);
        PerInterazioni linea9 = new PerInterazioni();
        linea9.turn(90);
        addObject(linea9,970,850);
    }
}
