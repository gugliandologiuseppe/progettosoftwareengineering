import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)
import java.util.List;
/**
 * Write a description of class Mountain3 here.
 * 
 * @author (your name) 
 * @version (a version number or a date)
 */
public class Mountain3 extends SWorld
{
    private GreenfootImage bg;
    /**
     * Constructor for objects of class Mountain2.
     * 
     */
    public Mountain3()
    {    
        // Create a new world with 600x400 cells with a cell size of 1x1 pixels.
        super(850, 600, 1, 2000, 1300); 
        prepare();
        super.act(); 
    }
    /**
     * Prepare the world for the start of the program.
     * That is: create the initial objects and add them to the world.
     */
    private void prepare(){ 
        oggSfondo();
        bloccaStrada();
        
        Checkpoint checkpoint = new Checkpoint(0, "Mountain3");
        addObject(checkpoint,-250,170);
        
        setMainActor(new Pg(), 100, 50);
        mainActor.setLocation(-470, 230);    
        bg = new GreenfootImage("rock2.png");
        setScrollingBackground(bg);
        
        Bat bat = new Bat();
        addObject(bat,600,150);
        Bat bat2 = new Bat();
        addObject(bat2,300,700);
    }
    public void act(){
        scrollObjects();
        scrollBackground();
        pause();
        nextWorld();
        stopWall();
        respawnFallPg();
    }
    /**
     * Classe che porta al mondo successivo
    */  
    private void nextWorld(){
        List<Pg> PgList = getObjects(Pg.class);
        Pg pg = PgList.get(0);       
        if(pg.atRightEdge()){
            Greenfoot.setWorld(new Shop(4));
        }
    } 
    /**
     * si ferma alla parente tornando indietro. Questo viene fatto con la linea trasparente 'PerInterazioni'
     */
    private void stopWall(){
        List<Pg> PgList = getObjects(Pg.class);
        Pg pg = PgList.get(0);
        if(pg.PgBarra()>0){
            pg.setLocation(pg.getX()+30,pg.getY());
        }
    }
    /**
     * Il pg prende danno da caduta e ritorna ad inizio livello.
     */
    private void respawnFallPg(){
        List<Pg> PgList = getObjects(Pg.class);
        Pg pg = PgList.get(0);
        if(pg.fallVoid()){
            mainActor.setLocation(-500, 312);
        }
    }
    /**
     * Oggetti creati per lo sfondo
     */
    private void oggSfondo(){
        Terra terra5 = new Terra(2);
        terra5.getImage().scale(terra5.getImage().getWidth() +300, terra5.getImage().getHeight() +320);
        addObject(terra5,-410,620);
        Terra terra = new Terra(4);
        addObject(terra,1100,910);
        
        Alberi albero = new Alberi(5);
        addObject(albero,-190,65);
        Pietre pietra = new Pietre(4);
        addObject(pietra,1200,780);
        Alberi albero2 = new Alberi(6);
        albero2.getImage().scale(albero2.getImage().getWidth() , albero2.getImage().getHeight() );
        addObject(albero2,930,680);
        BaseErba baseErba3 = new BaseErba(1);
        addObject(baseErba3,150,820);
        BaseErba baseErba4 = new BaseErba(1);
        addObject(baseErba4,300,550);
        BaseErba baseErba6 = new BaseErba(1);
        addObject(baseErba6,1200,500);
        BaseErba baseErba7 = new BaseErba(1);
        addObject(baseErba7,850,330);
        BaseErba baseErba5 = new BaseErba(1);
        addObject(baseErba5,600,330);
        BaseErba baseErba8 = new BaseErba(1);
        addObject(baseErba8,430,50);
        BaseErba baseErba9 = new BaseErba(1);
        addObject(baseErba9,-100,-160);
        BaseErba baseErba10 = new BaseErba(1);
        addObject(baseErba10,-500,70);
        
        Cassa cassa = new Cassa();
        addObject(cassa,550,265);
        Cassa cassa1 = new Cassa();
        addObject(cassa1,1250,442);

        Quadrifoglio quadrifoglio = new Quadrifoglio();
        addObject(quadrifoglio,100,750);
        Cartelli cartello = new Cartelli(1);
        addObject(cartello,1300,850);
        
        Spikes spike = new Spikes(1);
        spike.turn(180);
        addObject(spike, 700,280);
    }
    private void bloccaStrada(){
        PerInterazioni linea = new PerInterazioni();
        linea.turn(90);
        addObject(linea,5,600);
        PerInterazioni linea2 = new PerInterazioni();
        linea2.turn(90);
        addObject(linea2,5,500);
        PerInterazioni linea3 = new PerInterazioni();
        linea3.turn(90);
        addObject(linea3,5,700);
        PerInterazioni linea4 = new PerInterazioni();
        linea4.turn(90);
        addObject(linea4,5,800);
        PerInterazioni linea5 = new PerInterazioni();
        linea5.turn(90);
        addObject(linea5,5,900);
        PerInterazioni linea6 = new PerInterazioni();
        linea6.turn(90);
        addObject(linea6,5,400);
        PerInterazioni linea7 = new PerInterazioni();
        linea7.turn(90);
        addObject(linea7,5,300);
    }
}
