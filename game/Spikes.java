import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)

/**
 * Write a description of class Spikes here.
 * 
 * @author (your name) 
 * @version (a version number or a date)
 */
public class Spikes extends DangerousObject
{
    private int MAX_IMAGES = 4;
    private GreenfootImage[] spike = new GreenfootImage[MAX_IMAGES];
    
    public Spikes(int choice){
        images();
        switch(choice){
            case 1:
            spike[0].scale(spike[0].getWidth() -10, spike[0].getHeight() -3);
            setImage(spike[0]);
            break;
            case 2:
            setImage(spike[1]);
            break;
            case 3:
            setImage(spike[2]);
            break;
            case 4:
            setImage(spike[3]);
            break;
        }
    }
    private void images(){
        for (int i=0; i<MAX_IMAGES; i++){
            spike[i] = new GreenfootImage("punte_" +(i+1)+ ".png");
        }
        
    }
    /**
     * Act - do whatever the Spikes wants to do. This method is called whenever
     * the 'Act' or 'Run' button gets pressed in the environment.
     */
    public void act() 
    {
        // Add your action code here.
    }    
}
