import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)

/**
 * Write a description of class SelectLevel here.
 * 
 * @author (your name) 
 * @version (a version number or a date)
 */
public class SelectLevel extends World
{
    GreenfootImage bg = new GreenfootImage("sfondo_5.png");
    GreenfootImage cartello = new GreenfootImage("livelli.png");
    OggettiVari X1 = new OggettiVari(2);
    OggettiVari Lv1= new OggettiVari(4);
    OggettiVari Lv2= new OggettiVari(5);
    OggettiVari Lv3= new OggettiVari(6);
    
    
    /**
     * Constructor for objects of class SelectLevel.
     * 
     */
    public SelectLevel()
    {    
        // Create a new world with 600x400 cells with a cell size of 1x1 pixels.
        super(850, 600, 1);
        prepare();
    }
    private void prepare(){
        bg.scale(bg.getWidth()-680, bg.getHeight()-350);
        setBackground(bg);
        cartello.scale(cartello.getWidth()+100, cartello.getHeight()+100);
        getBackground().drawImage(cartello, 130, 0);
        
        addObject(X1,690,180);
        addObject(Lv1,305,236);
        
        //Accesso ai livelli sbloccarli quando completi il livello precedente
        Lv2.getImage().scale(Lv2.getImage().getWidth()+17, Lv2.getImage().getHeight()+17);
        addObject(Lv2,442,237);
        /**Lv3.getImage().scale(Lv3.getImage().getWidth()+17, Lv3.getImage().getHeight()+17);
        addObject(Lv3,582,237);
        */
    }
    
    public void act(){

        if(Greenfoot.mouseClicked(X1)){
            Greenfoot.setWorld(new StartMenu());
        }
    }
}
