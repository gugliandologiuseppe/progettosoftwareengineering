import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)
import java.util.List;
/**
 * Write a description of class MyWorld here.
 * 
 * @author (your name) 
 * @version (a version number or a date)
 */
public class Forest extends SWorld
{
    /**
     * Constructor for objects of class MyWorld.
     * 
     */
    public Forest()
    {    
        // Create a new world with 600x400 cells with a cell size of 1x1 pixels.
        super(850, 600, 1, 2500, 700); 
        prepare();
        super.act();
        
    }

    /**
     * Prepare the world for the start of the program.
     * That is: create the initial objects and add them to the world.
     */
    private void prepare()
    {      
        oggSfondo(); 
        tutorial();
        
        BaseErba baseErba2 = new BaseErba(0);
        addObject(baseErba2,900,190);
        BaseErba baseErba = new BaseErba(0);
        addObject(baseErba,464,370);

        Cassa cassa = new Cassa();
        addObject(cassa,1000,130);
        Checkpoint checkpoint = new Checkpoint(1, "Forest");
        addObject(checkpoint,-300,540);

        setMainActor(new Pg(), 100, 410);
        mainActor.setLocation(-300, 512);    //150 390
        GreenfootImage bg = new GreenfootImage("base.png");
        setScrollingBackground(bg);

        Slime slime = new Slime();
        addObject(slime,500,530);

               
    }
    
    public void act(){
        scrollObjects();
        scrollBackground();
        pause();
        nextWorld();
        
    }
    /**
     * Classe che porta al mondo successivo
    */  
    private void nextWorld(){
        List<Pg> PgList = getObjects(Pg.class);
        Pg pg = PgList.get(0);       
        if(pg.atRightEdge()){
            Greenfoot.setWorld(new Forest2());
        }
    } 
    /**
     * Oggetti creati per lo sfondo
     */
    private void oggSfondo(){
        Terra terra = new Terra(1);
        addObject(terra,140,563);
        Terra terra2 = new Terra(1);
        addObject(terra2,860,563);
        Terra terra3 = new Terra(1);
        addObject(terra3,1580,563);
        Terra terra4 = new Terra(1);
        addObject(terra4,-570,563);
        
        Pietre pietra = new Pietre(3);
        addObject(pietra,900,410);
        Alberi albero2 = new Alberi(2);
        addObject(albero2,-280,363);
        Cespugli cespuglio = new Cespugli(1);
        addObject(cespuglio,225,487);
        Alberi albero = new Alberi(1);
        addObject(albero,0,338);
        Cespugli cespuglio2 = new Cespugli(1);
        addObject(cespuglio2,755,487);
        Alberi albero3 = new Alberi(3);
        addObject(albero3,600,300);
        Cespugli cespuglio3 = new Cespugli(2);
        addObject(cespuglio3,-380,487);
        Alberi albero5 = new Alberi(3);
        addObject(albero5,-500,300);
        Alberi albero4 = new Alberi(1);
        addObject(albero4,-650,338);
        Cespugli cespuglio4 = new Cespugli(5);
        addObject(cespuglio4,1280,400);
        Alberi albero6 = new Alberi(1);
        addObject(albero6,1100,338);
        Alberi albero7 = new Alberi(3);
        addObject(albero7,1380,300);
        Cespugli cespuglio5 = new Cespugli(3);
        addObject(cespuglio5,-213,495);
        
        Cartelli cartello = new Cartelli(1);
        addObject(cartello,1565,473);
    }
    /**
     * 
     */
    public void tutorial(){
        Scritte scritta1 = new Scritte(2);
        addObject(scritta1,-170,410);
        
        Scritte scritta2 = new Scritte(3);
        addObject(scritta2,300,260);
    }
}
