import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)

/**
 * Write a description of class StartMenu here.
 * 
 * @author (your name) 
 * @version (a version number or a date)
 */
public class StartMenu extends World
{
    GreenfootImage bg = new GreenfootImage("sfondo_2.png");
    GreenfootImage cartello = new GreenfootImage("start.png");
    
    /**
     * Constructor for objects of class StartMenu.
     * 
     */
    public StartMenu()  //int choice
    {    
        // Create a new world with 600x400 cells with a cell size of 1x1 pixels.
        super(850, 600, 1);
        prepare();
    }
    private void prepare(){
        bg.scale(bg.getWidth()-730, bg.getHeight()-400);
        setBackground(bg);
        cartello.scale(cartello.getWidth()-80, cartello.getHeight()-70);
        getBackground().drawImage(cartello, 160, 0);
        
        Scritte();
        
        /**if(Greenfoot.mouseClicked(scritta)){ //funziona solo se usato nella classe Bho!!
            showText("Cose2: ", 100, 300);      
        } else showText("Cose: ", 100, 400); **/
    }
    private void Scritte(){
        Scritte scritta = new Scritte(1);
        scritta.getImage().scale(scritta.getImage().getWidth()-80, scritta.getImage().getHeight()-60);
        addObject(scritta,350,220);
        
        Scritte scritta2 = new Scritte(4);
        scritta2.getImage().scale(scritta2.getImage().getWidth()-140, scritta2.getImage().getHeight()-110);
        addObject(scritta2,550,283);
        
        Scritte scritta3 = new Scritte(5);
        scritta3.getImage().scale(scritta3.getImage().getWidth()-70, scritta3.getImage().getHeight()-50);
        addObject(scritta3,360,365);
    }
    public void act(){
        
    }
}
