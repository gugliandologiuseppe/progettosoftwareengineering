import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)
import java.util.List;
/**
 * Write a description of class InnerShop here.
 * 
 * @author (your name) 
 * @version (a version number or a date)
 */
public class InnerShop extends World
{
    GreenfootImage bg = new GreenfootImage("muro.jpg");
    GreenfootImage bacheca = new GreenfootImage("bacheca.png");
    GreenfootImage mago = new GreenfootImage("mago.png");
    GreenfootImage oggetto = new GreenfootImage("shop_oggetto.png");
    GreenfootImage scritta_shop = new GreenfootImage("scritta_shop.png");
    GreenfootImage soldOut = new GreenfootImage("soldOut.png");
    
    public World lastWorld;
    OggettiVari X = new OggettiVari(2);
    Pg pg=new Pg();
    Shots shot=new Shots();
    Calderone calderone= new Calderone();
    PerInterazioni press1 = new PerInterazioni();
    PerInterazioni press2 = new PerInterazioni();
    PerInterazioni press3 = new PerInterazioni(); 
    
    private int price1=0;
    private int price2=0;
    private int price3=0;
    private int numItem1=0;
    private int numItem2=0;
    private int numItem3=0;

    /**
     * Constructor for objects of class InnerShop.
     * 
     */
    public InnerShop(World world)// 
    {    
        // Create a new world with 600x400 cells with a cell size of 1x1 pixels.
        super(850, 600, 1); 
        bg.scale(bg.getWidth()-120, bg.getHeight()-126);
        setBackground(bg);
        lastWorld = world;
        
        prepare();
    }
    private void prepare()
    {
        imageWorld();
        //addObject(new Coin(),getX(),getY());
        addSellObject();
        addObject(X,770,230);
        buttonSell();
    }
    /**
     * Immagini inserite nel modno mondo
     */
    private void imageWorld(){
        bacheca.scale(bacheca.getWidth()+430, bacheca.getHeight()+280);
        bg.drawImage(bacheca,70,0);
        oggetto.scale(oggetto.getWidth()+70, oggetto.getHeight()+90);
        bg.drawImage(oggetto,150,270);
        bg.drawImage(oggetto,340,270);
        bg.drawImage(oggetto,530,270);
        mago.scale(mago.getWidth()-40, mago.getHeight()-60);
        bg.drawImage(mago,-5,300);
        scritta_shop.scale(scritta_shop.getWidth()+110, scritta_shop.getHeight()+60);
        bg.drawImage(scritta_shop,340,80);
        addObject(calderone,750,460);
        
        soldOut.scale(soldOut.getWidth()+70, soldOut.getHeight()+90);
    }
    /**
     * aggiunta dei tre oggetti da poter coprare
     */
    private void addSellObject(){
        ItemSell item1 = item(255,385);
        price1=item1.priceObject();
        showPrice(price1,250,460);
        numItem1=item1.numItems();
        showTextItem(numItem1,185,290);     
        
        ItemSell item2 = item(445,385);
        price2=item2.priceObject();
        showPrice(price2,440,460);
        numItem2=item2.numItems();
        showTextItem(numItem2,375,290);
        
        ItemSell item3 = item(635,385);
        price3=item3.priceObject();
        showPrice(price3,630,460);
        numItem3=item3.numItems();
        showTextItem(numItem3,565,290); 
    }
    /**
     * scelta di tre oggetti da inserire nello shop
     */
    private ItemSell item(int x, int y){
        int num = Greenfoot.getRandomNumber(11)+1;
        ItemSell item = new ItemSell(num);
        addObject(item,x,y);
        return item;
    }
    public void act(){
        pressX();
        buyItem();
    }
    /**
     * Tasto di uscita dallo shop
     */
    private void pressX(){
        if(Greenfoot.mouseClicked(X)){
            Greenfoot.setWorld(lastWorld);
        }
    }
    /**
     * mostra il prezzo dell'oggetto passato come parametro nella posizione indicata
     */
    private void showPrice(int prezzo,int x, int y){
        if(prezzo==5){
            GreenfootImage price5 = new GreenfootImage("5.png");
            bg.drawImage(price5,x,y);
        }else if(prezzo==10){
            GreenfootImage price10 = new GreenfootImage("10.png");
            bg.drawImage(price10,x,y);
        }else if(prezzo==15){
            GreenfootImage price15 = new GreenfootImage("15.png");
            bg.drawImage(price15,x,y);
        }else if(prezzo==20){
            GreenfootImage price20 = new GreenfootImage("20.png");
            bg.drawImage(price20,x,y);
        }else if(prezzo==30){
            GreenfootImage price30 = new GreenfootImage("30.png");
            bg.drawImage(price30,x,y);
        }
        
    }
    /**
     *  testo che compare in alto all'item a secondo del tipo di oggetto.
     */
    private void showTextItem(int numItem,int x, int y){
        if(numItem==1){
            GreenfootImage text = new GreenfootImage("atkMelee.png");
            bg.drawImage(text,x+1,y);
        }else if(numItem==2){
            GreenfootImage text = new GreenfootImage("Cura.png");
            bg.drawImage(text,x,y);
        }else if(numItem==3){
            GreenfootImage text = new GreenfootImage("shotAtk.png");
            bg.drawImage(text,x,y);
        }else if(numItem==4){
            GreenfootImage text = new GreenfootImage("speedUP.png");
            bg.drawImage(text,x,y);
        }else if(numItem>4){
            GreenfootImage text = new GreenfootImage("shotUp.png");
            bg.drawImage(text,x+11,y-7);
        }
        
    }
    /**
     * bottoni per permettere la vendita degli oggetti.
     */
    private void buttonSell(){
        press1.getImage().scale(press1.getImage().getWidth()-15, press1.getImage().getHeight() +25);
        press2.getImage().scale(press2.getImage().getWidth()-15, press2.getImage().getHeight() +25);
        press3.getImage().scale(press3.getImage().getWidth()-15, press3.getImage().getHeight() +25);
        
        addObject(press1,260,482);
        addObject(press2,450,482);
        addObject(press3,640,482);
    }
    /**
     * funzione ch diminuisce i soldi del Pg e fa comparire la scermata di soldout
     */
    private void buyItem(){
        int money=pg.moneyPg();
        seeMoney(money);

        List<ItemSell> itemList = getObjects(ItemSell.class);
        
        if(Greenfoot.mouseClicked(press1) && money>price1-1){
            pg.increaseMoney(-price1);
            itemList.get(0).getImage().setTransparency(0);
            getBackground().drawImage(soldOut, 150, 270);
            getBackground().drawImage(mago,-5,300);
            removeObject(press1);
            activeItem(numItem1);
        }else{  //suono che non può comprarlo
        }
        if(Greenfoot.mouseClicked(press2) && money>price2-1){
            pg.increaseMoney(-price2);
            itemList.get(1).getImage().setTransparency(0);
            getBackground().drawImage(soldOut, 340, 270);
            removeObject(press2);
            activeItem(numItem2);
        }else{ //suono 
        }
        if(Greenfoot.mouseClicked(press3) && money>price3-1){
            pg.increaseMoney(-price3);
            itemList.get(2).getImage().setTransparency(0);
            getBackground().drawImage(soldOut, 530, 270);
            removeObject(press3);
            activeItem(numItem3);
        }else{ //suono
        }
    }
    /**
     * mostrare le monete nello shop
     */
    private void seeMoney(int money){
        GreenfootImage coin = new GreenfootImage("coin_1.png");
        coin.scale(coin.getWidth()-5, coin.getHeight()-5);
        getBackground().drawImage(coin, 30, 50);
        showText("" +money, 80, 65);
    }
    /**
     * Cambiare lo Shot se selezionato.
     */
    private void activeItem(int numItem){
        switch(numItem){
            case 1:
                pg.increaseAttackDamage(1);
            break;
            case 2:
                pg.increaseLife(3.0);
            break;
            case 3:
                pg.increaseShotDamage(1);
            break;
            case 4:
                pg.increaseSpeed(0.2);        
            break;
            case 5:
                shot.changeShot(1);
            break;
            case 6:
                shot.changeShot(2);
            break;
            case 7:
                shot.changeShot(3);
            break;
            case 8:
                shot.changeShot(4);
            break;
            case 9:
                shot.changeShot(5);
            break;
            case 10:
                shot.changeShot(6);
            break;
            case 11:
                shot.changeShot(7);
            break;
        }
       
    }
}
