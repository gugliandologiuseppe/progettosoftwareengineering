import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)
import java.util.List;
/**
 * Write a description of class Cassa here.
 * 
 * @author (your name) 
 * @version (a version number or a date)
 */
public class Cassa extends Actor
{   
    private int MAX_IMAGES = 5;
    private GreenfootImage[] chest = new GreenfootImage[MAX_IMAGES];
    private SimpleTimer timer = new SimpleTimer();
    private boolean timerRunning = false;

    
    public Cassa(){
        imagesScale(); 
        setImage(chest[0]);
        
    }
    private void imagesScale(){
        for (int i=0; i<MAX_IMAGES; i++){
            chest[i] = new GreenfootImage("cassa_" +(i+1)+ ".png");
            chest[i].scale(chest[i].getWidth() -110, chest[i].getHeight() -115);
        }
        
    }
    /**
     * Act - do whatever the Cassa wants to do. This method is called whenever
     * the 'Act' or 'Run' button gets pressed in the environment.
     */
    public void act() 
    {
        // Add your action code here.
        //String key = Greenfoot.getKey();    "f".equals(key)
        List<Pg> PgList = getObjectsInRange(100,Pg.class);  
        if(PgList.size()>0){
            animateChest();
        }  
    }
    /**
    * 
    */
    private void animateChest(){
        //delaychest
        int coordX=getX()-150;
        int coordY=getY();
        int number = Greenfoot.getRandomNumber(5)+1;
        List<Pg> PgList = getObjectsInRange(100,Pg.class); 
        
        if(timerRunning == false){
            timer.mark();
            timerRunning = true;
        }
        if(timer.millisElapsed() > 230 && timerRunning == true ){
            
            if(getImage()== chest[0]){
                setImage(chest[1]);
            }
            else if(getImage()== chest[1]){
                setImage(chest[2]);
            }
            else if(getImage()== chest[2]){
                setImage(chest[3]);
            }else if(getImage()== chest[3]){
                setImage(chest[4]);
            } else if(getImage()== chest[4]){
                SpawnCoin();
                SpawnHeart();
                Pg pg = PgList.get(0);
                pg.increaseScore(20);
                getWorld().removeObject(this);  //rimuovere dopo perchè senno getWorld() =null
            }
            
            timer.mark();
            timerRunning = false; 
        }
    }
    /**
     * Spawna le monete dopo che il baule si è aperto.
     */
    private void SpawnCoin(){
        int number = Greenfoot.getRandomNumber(5)+1;
        
        for(int i=0; i<number;i++){
            int randomX = Greenfoot.getRandomNumber(100)+50;
            getWorld().addObject(new Coin(),getX()-100+randomX,getY());
        }
        
    }
    /**
     * Spawna una probabilità del 10% di cuori
     */
    private void SpawnHeart(){
        int number = Greenfoot.getRandomNumber(10);
        
        if(number<2){  //20% di probabilità
            int randomX = Greenfoot.getRandomNumber(100)+50;
            getWorld().addObject(new Vita(),getX()-100+randomX,getY());
        }
        
    }
    /**
     * 
     */
    private void IncreaseScorePg(){
    }
}
