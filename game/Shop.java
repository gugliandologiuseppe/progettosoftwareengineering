import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)
import java.util.List;

/**
 * Write a description of class Shop here.
 * 
 * @author (your name) 
 * @version (a version number or a date)
 * int di preview
 */
public class Shop extends SWorld
{
    private GreenfootImage bg;
    private int previewLevel=0;
    private int level=0;
    /**
     * Constructor for objects of class Shop.
     * 
     */
    public Shop(int level)
    {    
        // Create a new world with 600x400 cells with a cell size of 1x1 pixels.
        super(850, 600, 1, 1500, 700);
        previewLevel=level;
        prepare();
        super.act();
    }
    private void prepare()
    {  
        Terra terra = new Terra(previewLevel);
        addObject(terra,140,563);
        Terra terra2 = new Terra(previewLevel);
        addObject(terra2,860,563);
        Terra terra3 = new Terra(previewLevel);
        addObject(terra3,1580,563);
        Terra terra4 = new Terra(previewLevel);
        addObject(terra4,-570,563); 

        object();
        
        Pg pg = new Pg();
        setMainActor(pg, 100, 410);
        mainActor.setLocation(-100, 512);    //150 390
        if(previewLevel ==4){
            bg = new GreenfootImage("rock2.png");
        }else{
            bg = new GreenfootImage("base.png");
        }
        
        setScrollingBackground(bg);
        
    }
    /**
     * oggeti presenti nel mondo
     */
    private void object(){
        if(previewLevel ==4 ){
            Alberi albero2 = new Alberi(6);
            addObject(albero2,980,330);
            OggettiVari shop = new OggettiVari(1);
            addObject(shop,500,290);
            Cespugli cespuglio0 = new Cespugli(6);
            addObject(cespuglio0,-200,480);
            Alberi albero = new Alberi(4);
            albero.getImage().scale(albero.getImage().getWidth() +20, albero.getImage().getHeight() +50);
            addObject(albero,-70,300);
            
            level=2;
        }else {
            Alberi albero3 = new Alberi(2);
            addObject(albero3,900,338);
            OggettiVari shop = new OggettiVari(1);
            addObject(shop,500,290);
            Pietre pietra = new Pietre(5);
            addObject(pietra,1000,430);
            Alberi albero2 = new Alberi(3);
            addObject(albero2,-280,300);
            Alberi albero = new Alberi(1);
            addObject(albero,0,338);
            Cespugli cespuglio5 = new Cespugli(3);
            addObject(cespuglio5,-213,495);
            
            level=1;
        }
        
        Cartelli cartello = new Cartelli(1);
        addObject(cartello,1100,473);
        PerInterazioni barra = new PerInterazioni();
        addObject(barra,660,490);
    }
    
    public void act(){
        scrollObjects();
        scrollBackground();
        pause();
        nextWorld();

        List<Pg> PgList = getObjects(Pg.class);
        Pg pg = PgList.get(0);
        List<OggettiVari> OggVari = getObjects(OggettiVari.class);
        OggettiVari shop= OggVari.get(0);
        GreenfootImage img = shop.getImage();
        if(pg.PgBarra()>0){
            
            GreenfootImage pressF = new GreenfootImage("pressF.png");
            pressF.scale(pressF.getWidth()+10, pressF.getHeight()+10);
            img.drawImage(pressF,500,330);
            if(Greenfoot.isKeyDown("f")){
                World gameworld = this;
                Greenfoot.setWorld(new InnerShop(gameworld));//
            }

        }
    }
    /**
     * prossimo mondo da raggiungere passando dal limite destro
     */
    private void nextWorld(){
        List<Pg> PgList = getObjects(Pg.class);
        Pg pg = PgList.get(0);       
        if(pg.atRightEdge()){
            Greenfoot.setWorld(new LevelComplete(pg.getScore(),pg.getCollectible(), level));
        }
    } 
}
