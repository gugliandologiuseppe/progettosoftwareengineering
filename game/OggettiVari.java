import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)

/**
 * Write a description of class OggettiVari here.
 * 
 * @author (your name) 
 * @version (a version number or a date)
 */
public class OggettiVari extends Actor
{
    private int MAX_IMAGES = 7;
    private GreenfootImage[] object = new GreenfootImage[MAX_IMAGES];
    /**
     * Ogg1- Shop
     * Ogg2- X per Shop
     * Ogg3- freccia indietro
     * Ogg4- cerchio LV1 trasparente
     * Ogg5- cerchio Lv2 sbloccato
     * Ogg6- cerchio Lv3 sbloccato
     * Ogg7- freccia piantata a terra
     */    
    
    public OggettiVari(int choice){
        imagesScale();
        switch(choice){
            case 1:
            //pietre[0].scale(pietre[0].getWidth() -60, pietre[0].getHeight() -70);
            setImage(object[0]);
            break;
            case 2:
            setImage(object[1]);
            break;
            case 3:
            setImage(object[2]);
            break;
            case 4:
            object[3].setTransparency(0);
            setImage(object[3]);
            break;
            case 5:
            setImage(object[4]);
            break;
            case 6:
            setImage(object[5]);
            break;
            case 7:
            setImage(object[6]);
            break;
        }
            
        
    }
    private void imagesScale(){
        for (int i=0; i<MAX_IMAGES; i++){
            object[i] = new GreenfootImage("obj_" +(i+1)+ ".png");
        }
        
    }
    /**
     * Act - do whatever the OggettiVari wants to do. This method is called whenever
     * the 'Act' or 'Run' button gets pressed in the environment.
     */
    public void act() 
    {
        startLevel();
    }
    /**
     * Azioni pulsanti di selezione livello dei livelli.
     */ 
    private void startLevel(){
        if(Greenfoot.mouseClicked(this) && getImage()==object[3]){
            resetPg();
            Greenfoot.setWorld(new Forest());
        }
        if(Greenfoot.mouseClicked(this) && getImage()==object[4]){
            resetPg();
            Greenfoot.setWorld(new Mountain());
        }
    }
    /**
     * reset del personaggio dopo la morte, in questo caso resettiamo tutto per permettere un Restart da capo 
     * vita=3, collezionabili= 0, monete=0 e score=0; shot iniziale
     */
    private void resetPg(){
        Pg pg=new Pg();
        pg.resetPg(3.0,0,0,0,0);
    }
}
