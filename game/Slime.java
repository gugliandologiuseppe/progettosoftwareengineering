import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)
import java.util.List;
/**
 * Write a description of class Slime here.
 * 
 * @author (your name) 
 * @version (a version number or a date)
 */
public class Slime extends Enemy
{
    private int MAX_IMAGES = 4;
    private GreenfootImage[] slime_stay = new GreenfootImage[MAX_IMAGES];
    private GreenfootImage[] slime_right = new GreenfootImage[MAX_IMAGES];
    private GreenfootImage[] slime_left = new GreenfootImage[MAX_IMAGES];
    private GreenfootImage[] slime_death = new GreenfootImage[MAX_IMAGES];
    // PROVARE .mirrorHorizontally() crea immagine opposta
    
    
    
    private SimpleTimer timer = new SimpleTimer();
    private boolean timerRunning;
    private int action=0;
    private SimpleTimer timer2 = new SimpleTimer();
    private boolean timer2Running;
    private SimpleTimer timer3 = new SimpleTimer();
    private boolean timer3Running = false;
    private double speed= 0.8;
    public Slime(){
        life = 5;
        imagesAnimate();
        setImage(slime_stay[0]);
    }
    
    private void imagesAnimate(){
        for (int i=0; i<MAX_IMAGES; i++){
            slime_stay[i] = new GreenfootImage("slime_" +(i+1)+ ".png");
            slime_right[i] = new GreenfootImage("slime_right_" +(i+1)+ ".png");
            slime_left[i] = new GreenfootImage("slime_left_" +(i+1)+ ".png");
            slime_death[i] = new GreenfootImage("slime_death_" +(i+1)+ ".png");
            
            slime_stay[i].scale(slime_stay[i].getWidth() + 15, slime_stay[i].getHeight() +15);
            slime_right[i].scale(slime_right[i].getWidth() + 15, slime_right[i].getHeight() +15);
            slime_left[i].scale(slime_left[i].getWidth() + 15, slime_left[i].getHeight() +15);
            slime_left[i].scale(slime_death[i].getWidth() + 15, slime_death[i].getHeight() +15);
        }
    }
    
    
    
    /**
     * Act - do whatever the Slime wants to do. This method is called whenever
     * the 'Act' or 'Run' button gets pressed in the environment.
     */
    public void act() 
    {
        //delayStay();
        actionSlime();
    }
    // azioni che può fare 
    private void actionSlime(){
        if(isDying()) {
            die();
            return;
        }
        
        if(timerRunning == false){
            timer.mark();
            timerRunning = true;
        }
        
        if(timer.millisElapsed() > 5000 && timer.millisElapsed() <10000 && timerRunning == true ){
            delayRight();
        }else if(timer.millisElapsed() > 15000 && timer.millisElapsed() <20000 && timerRunning == true ){
            delayLeft();
        }else if(timer.millisElapsed()>20000 && timerRunning == true){
            timer.mark();
            timerRunning = false;
        }else delayStay();
        
    }
    // create Timer for dalay
    private void startTimer(){
        if(timer2Running == false){
            timer2.mark();
            timer2Running = true;
        }
    }
    //azione di sosta
    private void delayStay(){
        startTimer();
        if (timer2.millisElapsed() > 200 && timer2Running == true ){
            stay();
            timer2.mark();
            timer2Running = false;
        }
    }
    private void stay(){
        if(getImage()== slime_stay[0]){
            setImage(slime_stay[1]);
        }
        else if(getImage()== slime_stay[1]){
            setImage(slime_stay[2]);
        }
        else if(getImage()== slime_stay[2]){
            setImage(slime_stay[3]);
        } else{
            setImage(slime_stay[0]);
        }
    }
    //azione camminare a destra
    private void delayRight(){
        setLocation(getX()+ speed,getY());
        startTimer();
        if (timer2.millisElapsed() > 200 && timer2Running == true ){
            right_action();
            timer2.mark();
            timer2Running = false;
        }
    }
    private void right_action(){
        if(getImage()== slime_right[0]){
            setImage(slime_right[1]);
        }
        else if(getImage()== slime_right[1]){
            setImage(slime_right[2]);
        }
        else if(getImage()== slime_right[2]){
            setImage(slime_right[3]);
        } else{
            setImage(slime_right[0]);
        }
    }
    //azione camminare a sinistra
    private void delayLeft(){
        setLocation(getX()- speed,getY());
        startTimer();
        if (timer2.millisElapsed() > 200 && timer2Running == true ){
            left_action();
            timer2.mark();
            timer2Running = false;
        }
    }
    private void left_action(){
        if(getImage()== slime_left[0]){
            setImage(slime_left[1]);
        }
        else if(getImage()== slime_left[1]){
            setImage(slime_left[2]);
        }
        else if(getImage()== slime_left[2]){
            setImage(slime_left[3]);
        } else{
            setImage(slime_left[0]);
        }
    }
    
    protected void die(){
        if(timer3Running == false){
            timer3.mark();
            timer3Running = true;
        }
        if(timer3.millisElapsed() > 230 && timer3Running == true ){
            
            if(getImage() == slime_death[0]){
                setImage(slime_death[1]);
            }
            else if(getImage() == slime_death[1]){
                setImage(slime_death[2]);
            }
            else if(getImage() == slime_death[2]){
                setImage(slime_death[3]);
            } else if(getImage() == slime_death[3]){
                increaseScore();
                getWorld().removeObject(this);
            } else {
                setImage(slime_death[0]);
            }
            
            timer3.mark();
            timer3Running = false; 
        }
    }
    /**
     * Aumenta lo Score del Pg se ucciso
     */
    private void increaseScore(){
        List<Pg> PgList = getWorld().getObjects(Pg.class);  
        Pg pg = PgList.get(0);
        pg.increaseScore(50);
    }
}
