import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)
import java.util.List;
/**
 * Write a description of class Checkpoint here.
 * 
 * @author (your name) 
 * @version (a version number or a date)
 * 
 * type=0 normale
 * type=1 Trasparente
 */
public class Checkpoint extends Actor
{
    private int MAX_IMAGES = 2;
    private GreenfootImage[] checkpoint = new GreenfootImage[MAX_IMAGES];
    private SimpleTimer timer = new SimpleTimer();
    private boolean timerRunning = false;
    private int choice;
    private String lv;
    public Checkpoint(int type, String lv){
        choice=type;
        this.lv = lv;
        images();
        if(choice ==1){
            checkpoint[0].setTransparency(0);
            setImage(checkpoint[0]);
        }else{
            setImage(checkpoint[0]);
        } 
    }   
    private void images(){
        for (int i=0; i<MAX_IMAGES; i++){
            checkpoint[i] = new GreenfootImage("checkpoint_" +(i+1)+ ".png");
            //checkpoint[i].scale(checkpoint[i].getWidth() , checkpoint[i].getHeight() );
        }
    }
    /**
     * Act - do whatever the Checkpoint wants to do. This method is called whenever
     * the 'Act' or 'Run' button gets pressed in the environment.
     */
    public void act() 
    {
        active();
    } 
    /**
     * attiva il checkpoint
     */
    private void active(){
        List<Pg> PgList = getObjectsInRange(60,Pg.class);   
        if (PgList.size()>0){
            if(timerRunning == false){
                timer.mark();
                timerRunning = true;
            }
            if(timer.millisElapsed() > 200 && timerRunning == true ){
                Pg pg = PgList.get(0);
                setImage(checkpoint[1]);
                    //renderlo trasparente
                if(choice == 1){
                    getImage().setTransparency(0);
                } else {
                    //pg.saveCheckpoint(lv);
                }
                pg.saveCheckpoint(lv);
                //ATTIVATO
                
                timer.mark();
                timerRunning = false; 
            }
        }
    }
}
