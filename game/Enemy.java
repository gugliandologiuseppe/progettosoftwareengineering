import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)

/**
 * Write a description of class Enemy here.
 * 
 * @author (your name) 
 * @version (a version number or a date)
 */
public abstract class Enemy extends SmoothMover
{
    protected int life = 0;
    
    public void takeDamage(int damage){
        life -= damage;
    }
    
    public boolean isDying() {
        return life <= 0;
    }
    
    protected abstract void die();
}
