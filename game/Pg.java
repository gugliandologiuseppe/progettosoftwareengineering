import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)
import java.io.ObjectOutputStream;
import java.io.FileOutputStream;
import java.io.FileWriter;
import com.google.gson.Gson;
import java.io.Writer;

/**
 * Write a description of class Pg here.
 * 
 * @author (your name) 
 * @version (a version number or a date)
 */
public class Pg extends SmoothMover
{
    private int MAX_IMAGES = 4;
    private GreenfootImage[] pg1 = new GreenfootImage[MAX_IMAGES];
    private GreenfootImage[] right = new GreenfootImage[MAX_IMAGES-1];
    private GreenfootImage[] left = new GreenfootImage[MAX_IMAGES-1];
    private GreenfootImage[] simpleJump = new GreenfootImage[MAX_IMAGES-1];
    private GreenfootImage[] attackX_right = new GreenfootImage[MAX_IMAGES+1];
    private GreenfootImage[] attackX_left = new GreenfootImage[MAX_IMAGES+1];
    private GreenfootImage[] hearts = new GreenfootImage[MAX_IMAGES-2];
    private GreenfootImage[] t_damage = new GreenfootImage[MAX_IMAGES-2];
    private GreenfootImage[] shot = new GreenfootImage[MAX_IMAGES-2];
    private GreenfootImage[] death = new GreenfootImage[MAX_IMAGES];
    
    public CheckpointData checkpointData;
    
    private SimpleTimer timer = new SimpleTimer();
    private SimpleTimer timer2 = new SimpleTimer();
    private double speed = 2.8;
    private int attackDamage = 5;
    private int shotDamage = 5;
    private boolean isCurrentDamaged = false;
    private boolean timerRunning;
    private boolean timer2Running;
    private boolean wait_button = false;    //delay per un tasto premuto
    private boolean onGround= false;
    private double gravity = 0.0;
    private boolean jumping;
    private boolean attack;
    private double attack_speed= 0.8;
    public static double life =3.0;
    private boolean take_damage;    //if take damage
    private boolean lastTypedForward = true;
    private int damageIndex = -1;
    private boolean timeShot=false;
    public static int MONEY=0;
    public static int COLLECTIBLE=0;
    public static int SCORE=0;
    
    public Pg(){
        imagesAnimatePg();
        setImage(pg1[0]);
        
    }
    
    /**
     *  creare e inizializzare le immagini, vengono anche ingrandite
     */
    private void imagesAnimatePg(){
        for (int i=0; i<MAX_IMAGES; i++){
            pg1[i] = new GreenfootImage("pg1_" +(i+1)+ ".png");
            pg1[i].scale(pg1[i].getWidth() + 10, pg1[i].getHeight() +20);
            
            death[i] = new GreenfootImage("pg1_dead_" +(i+1)+ ".png");
            death[i].scale(death[i].getWidth() + 10, death[i].getHeight() +20);
        }
        
        for (int i=0; i<MAX_IMAGES-1; i++){
            right[i] = new GreenfootImage("pg1_right_" +(i+1)+ ".png");
            left[i] = new GreenfootImage("pg1_left_" +(i+1)+ ".png");
            simpleJump[i] = new GreenfootImage("pg1_salto_" +(i+1)+ ".png");
            
            right[i].scale(right[i].getWidth() + 10, right[i].getHeight() +20);
            left[i].scale(left[i].getWidth() + 10, left[i].getHeight() +20);
             simpleJump[i].scale(simpleJump[i].getWidth() + 10, simpleJump[i].getHeight() +20);
            
        }
        
        for(int i=0; i<MAX_IMAGES +1;i++){
            attackX_right[i] = new GreenfootImage("pg1_attaccoX_right_" +(i+1)+ ".png");
            attackX_right[i].scale(attackX_right[i].getWidth() + 10, attackX_right[i].getHeight() +20);
            
            attackX_left[i] = new GreenfootImage("pg1_attaccoX_left_" +(i+1)+ ".png");
            attackX_left[i].scale(attackX_left[i].getWidth() + 10, attackX_left[i].getHeight() +20);
        }
        
        for (int i=0; i<MAX_IMAGES-2; i++){
            hearts[i] = new GreenfootImage("cuore_" +(i+1)+ ".png");
            hearts[i].scale(hearts[i].getWidth()-75, hearts[i].getHeight()-70);
            
            t_damage[i] = new GreenfootImage("pg1_danno_" +(i+1)+ ".png");
            t_damage[i].scale(t_damage[i].getWidth() + 10, t_damage[i].getHeight() +20);
            
            shot[i] = new GreenfootImage("pg1_shot_" +(i+1)+ ".png");
            shot[i].scale(shot[i].getWidth() + 10, shot[i].getHeight() +20);
        }
    }
    /**
     * Get X for PG
     */
    public int PgX(){
        int x=getX();
        return x;
    }
    /**
     * controlla l'intersezione con la Barra Rossa
     */
    public int PgBarra(){
        int x=getObjectsInRange(30,PerInterazioni.class).size();
        return x;
    }
    /**
     * Act - do whatever the Pg wants to do. This method is called whenever
     * the 'Act' or 'Run' button gets pressed in the environment.
     */
    public void act() 
    {
        if(life>0){
            checkKey();
            checkForGround();
            checkFall();
            hearts();
            checkCoin();
            seeMoney();
            seeCollectible();
        } else {
            animateDeath();
        }
        //getWorld().showText("score: " + SCORE , 60, 290);
        //int ground = getWorld().getHeight() - getImage().getHeight();
        
        
    }
    /**
     * Azioni che può eseguir eil Personaggio
     */
    public void checkKey(){
        if(take_damage || takeDamage()){
            take_damage=true;
            if(timer2Running == false){
                timer2.mark();
                timer2Running = true;
            }

            if (timer2.millisElapsed() > 400 && timer2Running == true ){
                animateTakeDamage();
                timer2.mark();
                timer2Running = false;
                take_damage=false;
            }
            attack = false;
        }else if(Greenfoot.isKeyDown("right") && !take_damage){
            lastTypedForward = true;
            if(Greenfoot.isKeyDown("z") && !attack){
                attack = true;
                attackRight();
            }else moveRight();
            isCurrentDamaged = false;
        }else if(Greenfoot.isKeyDown("left") && !take_damage){
            lastTypedForward = false;
            if(Greenfoot.isKeyDown("z") && !attack){
                attack = true;
                attackLeft();
            }else moveLeft();
            isCurrentDamaged = false;
        }else {
            attack = false;
            delayStay();
        }
        
        if(Greenfoot.isKeyDown("z") && !attack){    //decidere se fare l'attacco da fermo.. ma a destra solo? 
            attack = true;
            attackRight();
        }
        if(Greenfoot.isKeyDown("c") && !timeShot){ 
            timeShot = true;
            animateShot();
            attack = false;
        }
        if(Greenfoot.isKeyDown("up") && !wait_button && !jumping){ 
            simpleJump(); 
            wait_button = true;
            attack = false;
        }
        if(!Greenfoot.isKeyDown("up") && wait_button){  //fa non spammare il tasto
            wait_button = false;
            attack = false;
        }

    }
    
    /**
     * Timer usato per le azioni
     */
    private void startTimer(){
        if(timerRunning == false){
            timer.mark();
            timerRunning = true;
        }
    }
    
    /**
     *  Animazioni per il pg fermo
     */
    private void stayPg(){
        if(getImage()== pg1[0]){
            setImage(pg1[1]);
        }
        else if(getImage()== pg1[1]){
            setImage(pg1[2]);
        }
        else if(getImage()== pg1[2]){
            setImage(pg1[3]);
        } else{
            setImage(pg1[0]);
        }
    }
    private void delayStay(){
        if(take_damage || takeDamage()){
            take_damage=true;
            if(timer2Running == false){
                timer2.mark();
                timer2Running = true;
            }

            if (timer2.millisElapsed() > 400 && timer2Running == true ){
                animateTakeDamage();
                timer2.mark();
                timer2Running = false;
                take_damage=false;
            }
            return;
        }
        
        startTimer();
        if (timer.millisElapsed() > 200 && timerRunning == true ){
            stayPg();
            timer.mark();
            timerRunning = false;
        }
        
        isCurrentDamaged = false;
    }
    
    /**
     * Animazione per il movimento a destra con delay
     */
    private void moveRight(){
        attack = false;
        setLocation(getX()+ speed,getY());
        
        startTimer();
        if (timer.millisElapsed() > 100 && timerRunning == true ){
            animateRight();
            timer.mark();
            timerRunning = false;
        }
    }
    private void animateRight(){
        if(getImage() == right[0]){
            setImage(right[1]);
        }
        else if(getImage() == right[1]){
            setImage(right[2]);
        } else {
            setImage(right[0]);
        }
    }
    
    /**
     * Animazione per il movimento a sinistra con delay
     */
    private void moveLeft(){
        attack = false;
        setLocation(getX()- speed, getY());
        
        startTimer();
        if (timer.millisElapsed() > 100 && timerRunning == true ){
            animateLeft();
            timer.mark();
            timerRunning = false;
        }
    }
    private void animateLeft(){
        if(getImage() == left[0]){
             setImage(left[1]);
        }
         else if(getImage() == left[1]){
             setImage(left[2]);
        } else {
             setImage(left[0]);
        }
    }

    /**
     * Animazione e movimento del Salto
     */
    private void simpleJump(){
        //animateJump();
        startTimer();
        if (timer.millisElapsed() > 30 && timerRunning == true ){
            animateJump();
            timer.mark();
            timerRunning = false;
        }
        gravity=gravity- 15.0;
        jumping=true;
        fall();
    }
    private void animateJump(){     //non funziona -veloce-
        if(getImage()==simpleJump[0]){
            setImage(simpleJump[1]);
        }else if(getImage()==simpleJump[1]){
            setImage(simpleJump[2]); 
        } else setImage(simpleJump[0]);
    }
    /**
     * Caduta sia per il salto che per il vuoto (?)
     */
    private void fall(){
        setLocation(getX(),getY()+gravity);
        if(gravity<=10){
            gravity=gravity+0.5;
        }
        jumping=true;
    }
    /**
     * Rintraccia l'oggetto intersecato da pg e se esiste chiama moveToGround
     */
    public boolean checkForGround(){    // farlo per classe (Class cls) 
        int spriteHeight = getImage().getHeight();
        int lookForGround = (int) (spriteHeight/2 -28);  //+5
        int lookForPlatform = (int) (spriteHeight/2 +5);
 
        Actor ground = getOneObjectAtOffset(0, lookForGround, Terra.class); 
        Actor platform = getOneObjectAtOffset(0, lookForPlatform, BaseErba.class);
 
        if (ground != null){
            moveToGround(ground);
            return true;
        } else if(platform != null){
            moveToGround(platform);
            return true;
        }else {
            jumping = true;
            return false;
        }
    }
    /**
     * Permette di appoggiarsi sulla piattaforma
     */
    public void moveToGround(Actor ground){
        int pgPositionOffset = 38;
        int groundHeight = ground.getImage().getHeight();
        if(ground instanceof BaseErba) {
            pgPositionOffset = 0;
        }
        int newY = ground.getY() - (groundHeight + getImage().getHeight())/2 + pgPositionOffset;
 
        setLocation(getX(), newY);  //setLocation(getX(), newY);
        jumping = false;
    }
    /**
     * Fa Saltare se è appoggiato in una piattaforma stabilita su moveToGround()
     */
    public void checkFall(){
        if(checkForGround()){
            gravity = 0.0;
        } else {
            fall();
        }
    }
    /**public boolean onGround(){
        int spriteHeight = getImage().getHeight();
        int lookForGround = (int) (spriteHeight/2) +5;
 
        Actor ground = getOneObjectAtOffset(0, lookForGround, Terra.class);
 
        if(ground == null) {
            jumping = true;
            return false;
        } else {
            moveToGround(ground);
            return true;
        }
 
    }**/
    /**
     * Animazione dell'attacco a destra e sinistra
     */
    public void attackRight(){
        //setLocation(getX()+ 0.5, getY());
        startTimer();
        if (timer.millisElapsed() > 70 && timerRunning == true ){
            if(getImage() == attackX_right[0]){
                 setImage(attackX_right[1]);
            }else if(getImage() == attackX_right[1]){
                 setImage(attackX_right[2]);
            }else if(getImage() == attackX_right[2]){
                 setImage(attackX_right[3]);
            }else if(getImage() == attackX_right[3]){
                 setImage(attackX_right[4]);
            } else {
                 setImage(attackX_right[0]);
            }
            timer.mark();
            timerRunning = false;
        }
    }
    public void attackLeft(){
        startTimer();
        if (timer.millisElapsed() > 70 && timerRunning == true ){
            if(getImage() == attackX_left[0]){
                 setImage(attackX_left[1]);
            }else if(getImage() == attackX_left[1]){
                 setImage(attackX_left[2]);
            }else if(getImage() == attackX_left[2]){
                 setImage(attackX_left[3]);
            }else if(getImage() == attackX_left[3]){
                 setImage(attackX_left[4]);
            } else {
                 setImage(attackX_left[0]);
            }
            timer.mark();
            timerRunning = false;
        }
    }
    /**
     * Vita del Pg e visualizzazione dei suoi cuori.
     */
    public void hearts(){
        if(life <= 0) {
            //perdi la partita
            return;
        }
        int xHearts=0;
        for (int i=0; i<life;){
            i++;
            //se entro nell'if è con la virgola ed uscirò dal ciclo al prossimo controllo
            if (!((life == Math.floor(life)) && !Double.isInfinite(life)) && i>=life) {
                getWorld().getBackground().drawImage(hearts[1], 30+xHearts, 18);
                break; //controlla l'id della foto
            }
            getWorld().getBackground().drawImage(hearts[0], 30+xHearts, 18);
            xHearts+=40;
        }     
    }
    /**
     *  Funzione che aumenta la vita del PG
     */
    public void increaseLife(double x){
        life= life+x;
    }
    /**
     * Il Pg prende danno a contatto con una classe specifica.
     */
    public boolean takeDamage(){
        Enemy enemy = (Enemy)getOneIntersectingObject(Enemy.class);
        if(enemy != null) {
            if(enemy.isDying()) {
                return false;
            }
            
            if(attack || (Greenfoot.isKeyDown("z"))) {
                enemy.takeDamage(attackDamage);
                return false;
            } else if(attack) {
                return false;
            }
        }
        
        DangerousObject obj = (DangerousObject)
                getOneIntersectingObject(DangerousObject.class);
        
        if((enemy != null || obj != null) && !take_damage && !isCurrentDamaged){
            isCurrentDamaged = true;
            life-=0.5;
            hearts();

            return true;
        } else return false;
    }
    private void animateTakeDamage(){
        //se sto tornando indietro il pg dovrà indietreggiare in quella direzione
        setLocation(getX()- (!lastTypedForward ? -100.0: 100.0), getY());
        startTimer();
        if (timer.millisElapsed() > 400 && timerRunning == true ){
            if(damageIndex == 0){
                damageIndex = 1;
                setImage(new GreenfootImage(t_damage[1]));
            } else {
                damageIndex = 0;
                setImage(new GreenfootImage(t_damage[0]));
            }
            if(!lastTypedForward) {
                getImage().mirrorHorizontally();
            }
            timer.mark();
            timerRunning = false;
        }
        timerRunning = true;
    }
    /**
     * Animazione della morte del personaggio e visualizzazione schermata di Game Over
     */
    private void animateDeath(){
        startTimer();
        if (timer.millisElapsed() > 250 && timerRunning == true ){
            if(getImage() == death[0]){
                 setImage(death[1]);
            }else if(getImage() == death[1]){
                 setImage(death[2]);
            }else if(getImage() == death[2]){
                 setImage(death[3]);
            }else if(getImage() == death[3]){
                getWorld().removeObject(this);
                Greenfoot.setWorld(new GameOver());
            }else{
                setImage(death[0]);
            }
            
            timer.mark();
            timerRunning = false;
        }
    }
    /**
     * Controlla se il pg si trova al bordo destro
     */
    public boolean atRightEdge(){
        if(getX() > getWorld().getWidth() -3){
            return true;
        }else return false;
    }
    /**
     * Controlla se il pg si trova al bordo sinistro
     */
    public boolean atLeftEdge(){
        if(getX() < 5){
            return true;
        }else return false;
    }
    /**
     * Controlla il contatto con una moneta
     */
    public boolean checkCoin(){    
        // controlla la grandezza della lista che ritorna la funziona 
        // il range parte dal centro dell'immagine del pg
        int coin = getObjectsInRange(30,Coin.class).size(); 
 
        if (coin >0){ 
            // aggiungi soldi al conto
            return true;
        } else {
            return false;
        }
    }
    /**
     * Attacco da Shot
     */
    private void animateShot(){ //+ int valore quante animazioni
        startTimer();
        if (timer.millisElapsed() > 150 && timerRunning == true ){
            if(getImage() == shot[0]){
                 setImage(shot[1]);
                 attackShot();
            } else {
                 setImage(shot[0]);
            }
            timer.mark();
            timerRunning = false;
        }
        timeShot=false;
    }
    private void attackShot(){
        int max_distance=10;
        int distance_shot=30;
        Shots lancio=new Shots();
        getWorld().addObject(lancio,getX()+distance_shot,getY());
        int delayTimer=0;
        
        for(int i=0; i<max_distance;i++){
            Enemy enemy = (Enemy)lancio.getOneIntersectingObject(Enemy.class);
            if(enemy != null) {
                if(!enemy.isDying()) {
                    enemy.takeDamage(shotDamage);
                }
            }
            //distance_shot+=60;
            
            lancio.setLocation(lancio.getX()+distance_shot,lancio.getY());
            Greenfoot.delay(3);
        }
        // se l'xOgg-xPg= tot elimina ogg lancio, quindi definire distanza massima di lancio 
    }
    /**
     * Funzioni per get e increase coin per i vari attori.
     */
    public int moneyPg(){
        return MONEY;
    }
    public void increaseMoney(int x){
        MONEY= MONEY+x;
    }
    /**
     * Visualizzazione dei soldi nei mondi.
     */
    public void seeMoney(){
        GreenfootImage coin = new GreenfootImage("coin_1.png");
        coin.scale(coin.getWidth()-5, coin.getHeight()-5);
        getWorld().getBackground().drawImage(coin, 30, 50);
        getWorld().showText("" +MONEY, 80, 65);
    }
    /**
     * somma il valore come argomento ai Collezionabili
     */
    public void increaseCollectible(int x){
        COLLECTIBLE= COLLECTIBLE+x;
    }
    /**
     * Visualizzazione dei soldi nei mondi.
     */
    public void seeCollectible(){
        GreenfootImage quadrifoglio = new GreenfootImage("quadrifoglio_1.png");
        quadrifoglio.scale(quadrifoglio.getWidth()-25, quadrifoglio.getHeight()-25);
        getWorld().getBackground().drawImage(quadrifoglio, 130, 50);
        getWorld().showText("" +COLLECTIBLE, 180, 65);
    }
    /**
     * Danno di caduta nel vuoto del Pg
    */
    public boolean fallVoid(){
        if(getY()>590){
            if(!take_damage && !isCurrentDamaged){
                isCurrentDamaged = true;
                life-=0.5;
                hearts();
            }
            isCurrentDamaged=false;
            return true;
        }else return false;
    } 
    /**
     *  Funzione Per il reset del personaggio dopo la morte, inserire il valore della vita dei collezionabili e 
     *  dei soldi iniziali che il PG deve avere al prossimo Respawn
     *  (in seguito aggiungere danno, shot da usare e non so se altro)
     */
    public void resetPg(double vita, int collect, int soldi, int score, int shot){
        life= vita;
        COLLECTIBLE = collect;
        MONEY= soldi;
        SCORE= score;
        Shots shots=new Shots();
        shots.changeShot(shot);
    }
    
    public void resetPgFromCheckpointData(){
        life= checkpointData.life;
        COLLECTIBLE = checkpointData.collezionabili;
        MONEY= checkpointData.monete;
        SCORE= checkpointData.score;
        Shots shots=new Shots();
        shots.changeShot(checkpointData.choiceShot);
    }
    /**
     * Resetta lo score a 0
     */
    public void resetScore(){
        SCORE=0;
    }
    /**
     * aumenta la velocità
     */
    public void increaseSpeed(double num){
        speed += 0.2;
    }
    /**
     * aumenta l'attacco melee
     */
    public void increaseAttackDamage(int num){
        attackDamage+=num;
    }
    /**
     * aumenta l'attacco dello shot
     */
    public void increaseShotDamage(int num){
        shotDamage+=num;
    }
    /**
     * aumenta la score del pg
     */
    public void increaseScore(int num){
        SCORE+=num;
    }
    /**
     * Ritorna lo score
     */
    public int getScore(){
        return SCORE; 
    }
    /**
     * Ritorna il numero dei colleizonabili raccolti
     */
    public int getCollectible(){
        return COLLECTIBLE;
    }
    
    public void saveCheckpoint(String lv) {
        checkpointData = new CheckpointData();
        checkpointData.monete = MONEY;
        checkpointData.collezionabili = COLLECTIBLE;
        checkpointData.checkpoint =  lv;
        checkpointData.speed = speed;
        checkpointData.attackDamage = attackDamage;
        checkpointData.shotDamage = shotDamage;
        checkpointData.attackSpeed = attack_speed;
        checkpointData.life = life;
        checkpointData.choiceShot = Shots.choiceShot;
        checkpointData.score = SCORE;
        
        try {
            Gson gson = new Gson();
            Writer writer = new FileWriter("datiCheckpoint.json");
            gson.toJson(checkpointData, writer);
            writer.flush();
            writer.close();
        } catch(Exception ex) {}
    }
}
