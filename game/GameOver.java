import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)

/**
 * Write a description of class GameOver here.
 * 
 * @author (your name) 
 * @version (a version number or a date)
 */
public class GameOver extends World
{
    GreenfootImage bg = new GreenfootImage("GameOver.jpg");
    Scritte startMenu = new Scritte(7);
    Scritte restart = new Scritte(10);
    
    /**
     * Constructor for objects of class GameOver.
     * 
     */
    public GameOver()
    {    
        // Create a new world with 600x400 cells with a cell size of 1x1 pixels.
        super(850, 600, 1);
        prepare();
    }
    private void prepare(){
        bg.scale(bg.getWidth()-400, bg.getHeight()-440);
        setBackground(bg);
        
        addObject(startMenu,640,480);
        
        restart.getImage().scale(restart.getImage().getWidth()+80, restart.getImage().getHeight()+50);
        addObject(restart,220,480);
    }
    
    public void act(){
        
    }
    
}
