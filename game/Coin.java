import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)
import java.util.List;

/**
 * Write a description of class Coin here.
 * 
 * @author (your name) 
 * @version (a version number or a date)
 */
public class Coin extends Actor
{
    private int MAX_IMAGES = 4;
    private GreenfootImage[] coin = new GreenfootImage[MAX_IMAGES];
    private SimpleTimer timer = new SimpleTimer();
    private boolean timerRunning = false;
    private SimpleTimer timer2 = new SimpleTimer();
    private boolean timer2Running = false;
   
    public Coin(){
        imagesScale(); 
        setImage(coin[0]);
    }
    private void imagesScale(){
        for (int i=0; i<MAX_IMAGES; i++){
            coin[i] = new GreenfootImage("coin_" +(i+1)+ ".png");
            //coin[i].scale(coin[i].getWidth() , coin[i].getHeight() );
        }
        
    }
    /**
     * Act - do whatever the Coin wants to do. This method is called whenever
     * the 'Act' or 'Run' button gets pressed in the environment.
     */
    public void act() 
    {
        // Add your action code here.
        animateCoin();
        increaseCoinsPg();
    } 
    /**
     * Animazione della moneta spawnata
     */
    private void animateCoin(){
        //delaycoin
        if(timerRunning == false){
            timer.mark();
            timerRunning = true;
        }
        if(timer.millisElapsed() > 230 && timerRunning == true ){
            
            if(getImage()== coin[0]){
                setImage(coin[1]);
            }
            else if(getImage()== coin[1]){
                setImage(coin[2]);
            }
            else if(getImage()== coin[2]){
                setImage(coin[3]);
            }else{
                setImage(coin[0]);
            }
            
            timer.mark();
            timerRunning = false; 
        }
    }
    /**
     * aumenta il valore delle monete che il Pg possiede
     */
    private void increaseCoinsPg(){
        List<Pg> PgList = getObjectsInRange(30,Pg.class);   
        if (PgList.size()>0){
            if(timerRunning == false){
                timer.mark();
                timerRunning = true;
            }
            if(timer.millisElapsed() > 200 && timerRunning == true ){
                Pg pg = PgList.get(0);
                pg.increaseMoney(1);
                getWorld().removeObject(this);
                
                timer.mark();
                timerRunning = false; 
            }
        }
    }
}
