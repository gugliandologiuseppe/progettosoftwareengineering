import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)

/**
 * Write a description of class cespugli here.
 * 
 * @author (your name) 
 * @version (a version number or a date)
 */
public class Cespugli extends Actor
{
    private int MAX_IMAGES = 6;
    private GreenfootImage[] cespugli = new GreenfootImage[MAX_IMAGES];
    
    public Cespugli(int choice){
        imagesScale();
        switch(choice){
            case 1:
            cespugli[0].scale(cespugli[0].getWidth() -80, cespugli[0].getHeight() -50);
            setImage(cespugli[0]);
            break;
            case 2:
            cespugli[1].scale(cespugli[1].getWidth() -230, cespugli[1].getHeight() -150);
            setImage(cespugli[1]);
            break;
            case 3:
            cespugli[2].scale(cespugli[2].getWidth() -50, cespugli[2].getHeight() -80);
            setImage(cespugli[2]);
            break;
            case 4:
            cespugli[3].scale(cespugli[3].getWidth() , cespugli[3].getHeight() );
            setImage(cespugli[3]);
            break;
            case 5:
            cespugli[4].scale(cespugli[4].getWidth() -70, cespugli[4].getHeight() -70);
            setImage(cespugli[4]);
            break;
            case 6:
            cespugli[5].scale(cespugli[5].getWidth() -200, cespugli[5].getHeight() -100);
            setImage(cespugli[5]);
            break;
        }
            
        
    }
    private void imagesScale(){
        for (int i=0; i<MAX_IMAGES; i++){
            cespugli[i] = new GreenfootImage("cespuglio_" +(i+1)+ ".png");
        }
        
    }
    /**
     * Act - do whatever the cespugli wants to do. This method is called whenever
     * the 'Act' or 'Run' button gets pressed in the environment.
     */
    public void act() 
    {
        // Add your action code here.
    }    
}
