import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)
import java.util.List;
/**
 * Write a description of class Forest3 here.
 * 
 * @author (your name) 
 * @version (a version number or a date)
 */
public class Forest3 extends SWorld
{

    /**
     * Constructor for objects of class MyWorld.
     * 
     */
    public Forest3()
    {    
        // Create a new world with 600x400 cells with a cell size of 1x1 pixels.
        super(850, 600, 1, 2500, 850); 
        prepare();
        super.act();
        
    }

    /**
     * Prepare the world for the start of the program.
     * That is: create the initial objects and add them to the world.
     */
    private void prepare()
    {   
        
        oggSfondo(); 
        piattaforme();
        muroInvisibile();
        
        Cassa cassa = new Cassa();
        addObject(cassa,-140,-10);
        Cassa cassa2 = new Cassa();
        addObject(cassa2,1150,610);
        Checkpoint checkpoint = new Checkpoint(0, "Forest3");
        addObject(checkpoint,-450,540);
        
        setMainActor(new Pg(), 200, 410);
        mainActor.setLocation(-600, 612);    //150 390
        GreenfootImage bg = new GreenfootImage("base.png");
        setScrollingBackground(bg);
        
    }
    
    public void act(){
        scrollObjects();
        scrollBackground();
        pause();
        nextWorld();
        stopWall();
        respawnFallPg();
    }
    /**
     * Classe che porta al mondo successivo
    */  
    private void nextWorld(){
        List<Pg> PgList = getObjects(Pg.class);
        Pg pg = PgList.get(0);       
        if(pg.atRightEdge()){
            Greenfoot.setWorld(new Shop(1));
        }
    } 
    /**
     * si ferma alla parente tornando indietro. Questo viene fatto con la linea trasparente 'PerInterazioni'
     */
    private void stopWall(){
        List<Pg> PgList = getObjects(Pg.class);
        Pg pg = PgList.get(0);
        if(pg.PgBarra()>0 && Greenfoot.isKeyDown("right")){
            pg.setLocation(pg.getX()-30,pg.getY());
        }
        if(pg.PgBarra()>0 && Greenfoot.isKeyDown("left")){
            pg.setLocation(pg.getX()+30,pg.getY());
        }
    }
    /**
     * Il pg prende danno da caduta e ritorna ad inizio livello.
     */
    private void respawnFallPg(){
        List<Pg> PgList = getObjects(Pg.class);
        Pg pg = PgList.get(0);
        if(pg.fallVoid()){
            mainActor.setLocation(-600, 512);
        }
    }
    /**
     * Oggetti creati per lo sfondo
     */
    private void oggSfondo(){
        Terra terra5 = new Terra(2);
        addObject(terra5,-100,560);
        Quadrifoglio quadrifoglio1 = new Quadrifoglio();
        addObject(quadrifoglio1,1400,150);

        Terra terra2 = new Terra(3);
        addObject(terra2,143,720);
        Terra terra = new Terra(1);
        addObject(terra,140,663);
        Terra terra6 = new Terra(3);
        addObject(terra6,863,720);
        Terra terra4 = new Terra(1);
        addObject(terra4,-570,663);
        Terra terra7 = new Terra(1);
        addObject(terra7,860,663);
        Pietre pietra = new Pietre(4);
        addObject(pietra,-600,530);
        Alberi albero = new Alberi(3);
        addObject(albero,-750,420);
        Cespugli cespuglio = new Cespugli(2);
        addObject(cespuglio,-200,340);
        Cespugli cespuglio4 = new Cespugli(5);
        addObject(cespuglio4,-50,270);
        Pietre pietra2 = new Pietre(5);
        addObject(pietra2,280,550);
        Cespugli cespuglio2 = new Cespugli(4);
        addObject(cespuglio2,-140,530);
        Alberi albero2 = new Alberi(3);
        addObject(albero2,150,400);
        Cespugli cespuglio3 = new Cespugli(1);
        addObject(cespuglio3,100,590);
        Alberi albero3 = new Alberi(1);
        addObject(albero3,1000,435);
        Cartelli cartello = new Cartelli(1);
        addObject(cartello,1570,519);
        
        Spikes spike = new Spikes(1);
        spike.turn(180);
        addObject(spike, 900,630);
    }
    private void piattaforme(){
        BaseErba baseErba = new BaseErba(0);
        addObject(baseErba,-680,450);
        BaseErba baseErba2 = new BaseErba(0);
        addObject(baseErba2,-720,200);
        BaseErba baseErba3 = new BaseErba(0);
        addObject(baseErba3,500,450);
        BaseErba baseErba4 = new BaseErba(0);
        addObject(baseErba4,-205,50);
        BaseErba baseErba5 = new BaseErba(0);
        addObject(baseErba5,860,220);
        BaseErba baseErba6 = new BaseErba(0);
        addObject(baseErba6,1200,50);
        BaseErba baseErba7 = new BaseErba(0);
        addObject(baseErba7,500,50);
        BaseErba baseErba8 = new BaseErba(0);
        addObject(baseErba8,1670,600);
    }
    private void muroInvisibile(){
        PerInterazioni linea = new PerInterazioni();
        linea.turn(90);
        addObject(linea,-375,590);
        PerInterazioni linea2 = new PerInterazioni();
        linea2.turn(90);
        addObject(linea2,-375,490);
        PerInterazioni linea3 = new PerInterazioni();
        linea3.turn(90);
        addObject(linea3,-375,390);
        PerInterazioni linea4 = new PerInterazioni();
        linea4.turn(90);
        addObject(linea4,180,590);
        PerInterazioni linea5 = new PerInterazioni();
        linea5.turn(90);
        addObject(linea5,180,490);
        PerInterazioni linea6 = new PerInterazioni();
        linea6.turn(90);
        addObject(linea6,180,400);
    }
}
