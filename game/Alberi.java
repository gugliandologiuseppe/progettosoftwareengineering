import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)

/**
 * Write a description of class Alberi here.
 * 
 * @author (your name) 
 * @version (a version number or a date)
 */
public class Alberi extends Actor
{
    private int MAX_IMAGES = 6;
    private GreenfootImage[] alberi = new GreenfootImage[MAX_IMAGES];
    
    public Alberi(int choice){
        imagesScale();
        switch(choice){
            case 1:
            alberi[0].scale(alberi[0].getWidth() -60, alberi[0].getHeight() -70);
            setImage(alberi[0]);
            break;
            case 2:
            alberi[1].scale(alberi[1].getWidth() -20, alberi[1].getHeight() -20);
            setImage(alberi[1]);
            break;
            case 3:
            alberi[2].scale(alberi[2].getWidth() -60, alberi[2].getHeight() -250);
            setImage(alberi[2]);
            break;
            case 4:
            alberi[3].scale(alberi[3].getWidth() +50, alberi[3].getHeight() +100);
            setImage(alberi[3]);
            break;
            case 5:
            alberi[4].scale(alberi[4].getWidth() +80, alberi[4].getHeight() +80);
            setImage(alberi[4]);
            break;
            case 6:
            alberi[5].scale(alberi[5].getWidth() +80, alberi[5].getHeight() +100);
            setImage(alberi[5]);
            break;
        }
            
        
    }
    private void imagesScale(){
        for (int i=0; i<MAX_IMAGES; i++){
            alberi[i] = new GreenfootImage("albero_" +(i+1)+ ".png");
        }
        
    }
    
    /**
     * Act - do whatever the Alberi wants to do. This method is called whenever
     * the 'Act' or 'Run' button gets pressed in the environment.
     */
    public void act() 
    {
        // Add your action code here.
    }    
}
