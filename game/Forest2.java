import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)
import java.util.List;

/**
 * Write a description of class Forest2 here.
 * 
 * @author (your name) 
 * @version (a version number or a date)
 */
public class Forest2 extends SWorld
{
    public World gameWorld;
    /**
     * Constructor for objects of class MyWorld.
     * 
     */
    public Forest2()
    {    
        // Create a new world with 600x400 cells with a cell size of 1x1 pixels.
        super(850, 600, 1, 2500, 850); 
        prepare();
        super.act();
        gameWorld=this;
    }

    /**
     * Prepare the world for the start of the program.
     * That is: create the initial objects and add them to the world.
     */
    private void prepare()
    {   
        oggSfondo(); 
        piattaforme();

        Cassa cassa = new Cassa();
        addObject(cassa,-620,280);
        Quadrifoglio quadrifoglio = new Quadrifoglio();
        addObject(quadrifoglio,1630,210);
        
        setMainActor(new Pg(), 200, 410);
        mainActor.setLocation(-100, 612);    //150 390
        GreenfootImage bg = new GreenfootImage("base.png");
        setScrollingBackground(bg);
        
        Cespugli cespuglio4 = new Cespugli(1);
        addObject(cespuglio4,-440,610);
        Alberi albero = new Alberi(2);
        addObject(albero,-500,500);
        
    }
    
    public void act(){
        scrollObjects();
        scrollBackground();
        pause();
        nextWorld();
        stopWall();
        respawnFallPg();
    }
    /**
     * Classe che porta al mondo successivo
    */  
    private void nextWorld(){
        List<Pg> PgList = getObjects(Pg.class);
        Pg pg = PgList.get(0);       
        if(pg.atRightEdge()){
            Greenfoot.setWorld(new Forest3());
        }
    } 
    /**
     * si ferma alla parente tornando indietro. Questo viene fatto con la linea trasparente 'PerInterazioni'
     */
    private void stopWall(){
        List<Pg> PgList = getObjects(Pg.class);
        Pg pg = PgList.get(0);
        if(pg.PgBarra()>0){
            pg.setLocation(pg.getX()+30,pg.getY());
            
        }
    }
    /**
     * Il pg prende danno da caduta nel vuoto e ritorna ad inizio livello.
     */
    private void respawnFallPg(){
        List<Pg> PgList = getObjects(Pg.class);
        Pg pg = PgList.get(0);
        if(pg.fallVoid()){
            mainActor.setLocation(-100, 512);
        }
    }
    /**
     * Oggetti creati per lo sfondo
     */
    private void oggSfondo(){
        Terra terra5 = new Terra(2);
        addObject(terra5,-700,510);
        
        PerInterazioni linea = new PerInterazioni();
        linea.turn(90);
        addObject(linea,-430,600);
        PerInterazioni linea2 = new PerInterazioni();
        linea2.turn(90);
        addObject(linea2,-430,500);
        PerInterazioni linea3 = new PerInterazioni();
        linea3.turn(90);
        addObject(linea3,-430,300);
        PerInterazioni linea4 = new PerInterazioni();
        linea4.turn(90);
        addObject(linea4,-430,400);
        
        Terra terra2 = new Terra(3);
        addObject(terra2,143,720);
        Terra terra = new Terra(1);
        addObject(terra,140,663);
        Terra terra6 = new Terra(3);
        addObject(terra6,1578,720);
        Terra terra3 = new Terra(1);
        addObject(terra3,1580,663);
        Terra terra7 = new Terra(3);
        addObject(terra7,-568,720);
        Terra terra4 = new Terra(1);
        addObject(terra4,-570,663);
        
        Pietre pietra2 = new Pietre(5);
        addObject(pietra2,280,530);
        Pietre pietra = new Pietre(4);
        addObject(pietra,-600,530);
        Cespugli cespuglio = new Cespugli(2);
        addObject(cespuglio,-740,280);
        Cespugli cespuglio2 = new Cespugli(4);
        addObject(cespuglio2,-740,530);
        Alberi albero2 = new Alberi(3);
        addObject(albero2,150,400);
        Cespugli cespuglio3 = new Cespugli(1);
        addObject(cespuglio3,100,590);
        Alberi albero3 = new Alberi(1);
        addObject(albero3,1430,435);
        
        Cartelli cartello = new Cartelli(1);
        addObject(cartello,1565,573);
    }
    private void piattaforme(){
        BaseErba baseErba2 = new BaseErba(0);
        addObject(baseErba2,880,290);
        BaseErba baseErba = new BaseErba(0);
        addObject(baseErba,400,470);
        BaseErba baseErba3 = new BaseErba(0);
        addObject(baseErba3,390,100);
        BaseErba baseErba4 = new BaseErba(0);
        addObject(baseErba4,-155,290);
        BaseErba baseErba5 = new BaseErba(0);
        addObject(baseErba5,1350,100);
        BaseErba baseErba6 = new BaseErba(0);
        addObject(baseErba6,1700,300);
    }
    
}

