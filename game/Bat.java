import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)
import java.util.List;
/**
 * Write a description of class Bat here.
 * 
 * @author (your name) 
 * @version (a version number or a date)
 */
public class Bat extends Enemy
{
    private int MAX_IMAGES = 4;
    private GreenfootImage[] bat_stay = new GreenfootImage[MAX_IMAGES];
    private GreenfootImage[] bat_right = new GreenfootImage[MAX_IMAGES];
    private GreenfootImage[] bat_left = new GreenfootImage[MAX_IMAGES];
    private GreenfootImage bat_death = new GreenfootImage("bat_death.png");
    
    private SimpleTimer timer = new SimpleTimer();
    private boolean timerRunning;
    private SimpleTimer timer2 = new SimpleTimer();
    private boolean timer2Running;
    private SimpleTimer timer3 = new SimpleTimer();
    private boolean timer3Running;
    private double speed= 1.0;
    
    public Bat(){
        life = 5;
        imagesAnimate();
        setImage(bat_stay[0]);
    }
    
    private void imagesAnimate(){
        for (int i=0; i<MAX_IMAGES; i++){
            bat_stay[i] = new GreenfootImage("bat_" +(i+1)+ ".png");
            bat_left[i] = new GreenfootImage("bat_left_" +(i+1)+ ".png");
            bat_right[i] = new GreenfootImage("bat_right_" +(i+1)+ ".png");
            
            bat_stay[i].scale(bat_stay[i].getWidth() - 60, bat_stay[i].getHeight() -28);
            bat_left[i].scale(bat_left[i].getWidth() -60, bat_left[i].getHeight() -28);
            bat_right[i].scale(bat_right[i].getWidth() -60, bat_right[i].getHeight() -28);
        }
        bat_death.scale(bat_death.getWidth() - 45, bat_death.getHeight() -28);
    }
    /**
     * Act - do whatever the Bat wants to do. This method is called whenever
     * the 'Act' or 'Run' button gets pressed in the environment.
     */
    public void act() 
    {
        action();
    }
    /**
     * 
     */
    private void action(){
        if(isDying()) {
            die();
            return;
        }
        
        if(timerRunning == false){
            timer.mark();
            timerRunning = true;
        }
        
        if(timer.millisElapsed() > 3000 && timer.millisElapsed() <12000 && timerRunning == true ){
            right();
        }else if(timer.millisElapsed() > 15000 && timer.millisElapsed() <22000 && timerRunning == true ){
            delayLeft();
        }else if(timer.millisElapsed()>22000 && timerRunning == true){
            timer.mark();
            timerRunning = false;
        }else stay();
        
    }
    // create Timer for dalay
    private void startTimer(){
        if(timer2Running == false){
            timer2.mark();
            timer2Running = true;
        }
    }
    /**
     * animazione da fermo
     */
    private void stay(){
        startTimer();
        if (timer2.millisElapsed() > 200 && timer2Running == true ){
            if(getImage()== bat_stay[0]){
                setImage(bat_stay[1]);
            }
            else if(getImage()== bat_stay[1]){
                setImage(bat_stay[2]);
            }
            else if(getImage()== bat_stay[2]){
                setImage(bat_stay[3]);
            } else{
                setImage(bat_stay[0]);
            }
            timer2.mark();
            timer2Running = false;
        }
        
        
    }
    /**
     * animazione da destra
     */
    private void right(){
        setLocation(getX()+ speed,getY());
        startTimer();
        if (timer2.millisElapsed() > 200 && timer2Running == true ){
            if(getImage()== bat_right[0]){
                setImage(bat_right[1]);
            }
            else if(getImage()== bat_right[1]){
                setImage(bat_right[2]);
            }
            else if(getImage()== bat_right[2]){
                setImage(bat_right[3]);
            } else{
                setImage(bat_right[0]);
            }
            
            timer2.mark();
            timer2Running = false;
        }
    }
    /**
     * 
     */
    private void delayLeft(){
        setLocation(getX()- speed,getY());
        startTimer();
        if (timer2.millisElapsed() > 200 && timer2Running == true ){
            if(getImage()== bat_left[0]){
                setImage(bat_left[1]);
            }
            else if(getImage()== bat_left[1]){
                setImage(bat_left[2]);
            }
            else if(getImage()== bat_left[2]){
                setImage(bat_left[3]);
            } else{
                setImage(bat_left[0]);
            }
            timer2.mark();
            timer2Running = false;
        }
    }
    /**
     * Monte del nemico
     */
    protected void die(){
        if(timer3Running == false){
            timer3.mark();
            timer3Running = true;
        }
        if(timer3.millisElapsed() > 230 && timer3.millisElapsed() > 600 && timer3Running == true ){
            setImage(bat_death);
            setLocation(getX(),getY()+ speed);
        }if(timer3.millisElapsed() > 600 && timer3.millisElapsed() > 800 && timer3Running == true ){    
            setLocation(getX(),getY()+ speed);
        }if(timer3.millisElapsed() > 800 && timer3.millisElapsed() > 1000 && timer3Running == true ){
            setLocation(getX(),getY()+ speed);
        }if(timer3.millisElapsed() > 1000 && timer3.millisElapsed() > 1200 && timer3Running == true ){
            setLocation(getX(),getY()- speed);
            increaseScore();
            getWorld().removeObject(this);
        }if(timer3.millisElapsed() > 1200 && timer3Running == true ){
            timer3.mark();
            timer3Running = false; 
        }
        
        
    }
    /**
     * Aumenta lo Score del Pg se ucciso
     */
    private void increaseScore(){
        List<Pg> PgList = getWorld().getObjects(Pg.class);  
        Pg pg = PgList.get(0);
        pg.increaseScore(50);
    }
}
