import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)

/**
 * Write a description of class Cartelli here.
 * 
 * @author (your name) 
 * @version (a version number or a date)
 */
public class Cartelli extends Actor
{
    private int MAX_IMAGES = 2;
    private GreenfootImage[] cartelli = new GreenfootImage[MAX_IMAGES];
    
    public Cartelli(int choice){  
        imagesScale();
        switch(choice){
            case 1:
            cartelli[0].scale(cartelli[0].getWidth() -80, cartelli[0].getHeight() -80);
            setImage(cartelli[0]);
            break;
            case 2:
            cartelli[1].scale(cartelli[1].getWidth() , cartelli[1].getHeight() );
            setImage(cartelli[1]);
            break;
            case 3:

            break;
        }
            
        
    }
    private void imagesScale(){
        for (int i=0; i<MAX_IMAGES; i++){
            cartelli[i] = new GreenfootImage("cartello_" +(i+1)+ ".png");
        }
        
    }
    /**
     * Act - do whatever the Cartelli wants to do. This method is called whenever
     * the 'Act' or 'Run' button gets pressed in the environment.
     */
    public void act() 
    {
        // Add your action code here.
    }    
}
