import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)

/**
 * Write a description of class PauseMenu here.
 * 
 * @author (your name) 
 * @version (a version number or a date)
 */
public class PauseMenu extends World
{
    GreenfootImage bg = new GreenfootImage("sfondo_3.png");
    GreenfootImage bg2 = new GreenfootImage("sfondo_4.png");
    GreenfootImage menu = new GreenfootImage("MenuPause.png");
    
    public World lastWorld;
    /**
     * Constructor for objects of class PauseMenu.
     * 
     */
    public PauseMenu(World world)
    {    
        // Create a new world with 600x400 cells with a cell size of 1x1 pixels.
        super(850, 600, 1); 
        lastWorld = world;
        prepare();
    }
    private void prepare(){
        bg.scale(bg.getWidth()+100, bg.getHeight()-300);
        setBackground(bg);
        bg2.scale(bg2.getWidth()-1450, bg2.getHeight()-1320);
        bg.drawImage(bg2,-10,-10);
        
        menu.scale(menu.getWidth()+270, menu.getHeight()+290);
        getBackground().drawImage(menu, 200, -5);
        
        Scritte();
        
    }
    /**
     * Scritte dei tasti presenti.
     */
    private void Scritte(){
        Scritte scritta = new Scritte(6);
        scritta.getImage().scale(scritta.getImage().getWidth()-70, scritta.getImage().getHeight()-20);
        addObject(scritta,450,350);
        
        Scritte scritta2 = new Scritte(7);
        scritta2.getImage().scale(scritta2.getImage().getWidth()-120, scritta2.getImage().getHeight()-80);
        addObject(scritta2,450,440);
        
        Scritte scritta3 = new Scritte(8);
        scritta3.getImage().scale(scritta3.getImage().getWidth()-90, scritta3.getImage().getHeight()-20);
        addObject(scritta3,450,253);
    }
    /**
     * Ritorna il mondo precedente a questo.
     */
    public World getLastWorld()
    {
        return lastWorld;
    }
}
