import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)
import java.util.List;

/**
 * Write a description of class Quadrifoglio here.
 * 
 * @author (your name) 
 * @version (a version number or a date)
 */
public class Quadrifoglio extends Actor
{
    private int MAX_IMAGES = 2;
    private GreenfootImage[] quadrifoglio = new GreenfootImage[MAX_IMAGES];
    private SimpleTimer timer = new SimpleTimer();
    private boolean timerRunning = false;
    
    public Quadrifoglio(){
        images();
    }
    private void images(){
        for (int i=0; i<MAX_IMAGES; i++){
            quadrifoglio[i] = new GreenfootImage("quadrifoglio_" +(i+1)+ ".png");
            //quadrifoglio[i].scale(quadrifoglio[i].getWidth() , quadrifogio[i].getHeight() );
        }
        
    }
    /**
     * Act - do whatever the Quadrifoglio wants to do. This method is called whenever
     * the 'Act' or 'Run' button gets pressed in the environment.
     */
    public void act() 
    {
        animation();
        increaseCollectiblePg();
    }   
    /**
     * Animazione del collezionabile
     */
    private void animation(){
        //delaycoin
        if(timerRunning == false){
            timer.mark();
            timerRunning = true;
        }
        if(timer.millisElapsed() > 230 && timerRunning == true ){
            
            if(getImage()== quadrifoglio[0]){
                setImage(quadrifoglio[1]);
            }
            else{
                setImage(quadrifoglio[0]);
            }
            
            timer.mark();
            timerRunning = false; 
        }
    }
    /**
     * 
     */
    private void increaseCollectiblePg(){
        List<Pg> PgList = getObjectsInRange(40,Pg.class);   
        if (PgList.size()>0){
            if(timerRunning == false){
                timer.mark();
                timerRunning = true;
            }
            if(timer.millisElapsed() > 200 && timerRunning == true ){
                Pg pg = PgList.get(0);
                pg.increaseCollectible(1);
                pg.increaseScore(35);
                getWorld().removeObject(this);
                
                timer.mark();
                timerRunning = false; 
            }
        }
    }
}
