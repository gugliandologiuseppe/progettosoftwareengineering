import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)

/**
 * Write a description of class Shots here.
 * 
 * @author (your name) 
 * @version (a version number or a date)
 */
public class Shots extends Actor
{
    private int MAX_IMAGES = 8;
    private GreenfootImage[] shot = new GreenfootImage[MAX_IMAGES];
    private SimpleTimer timer = new SimpleTimer();
    private boolean timerRunning = false;
    public static int choiceShot=0;
    
    public Shots(){
        images();
        scalesImage();
        setImage(shot[choiceShot]);
    }
    private void images(){
        for (int i=0; i<MAX_IMAGES; i++){
            shot[i] = new GreenfootImage("shot" +(i+1)+ ".png");
        }
    }
    private void scalesImage(){
        shot[0].scale(shot[0].getWidth()+20, shot[0].getHeight()+20);
        shot[1].scale(shot[1].getWidth()+25, shot[1].getHeight()+15);
        shot[2].scale(shot[2].getWidth()+25, shot[2].getHeight()+25);
        shot[3].scale(shot[3].getWidth()-25, shot[3].getHeight()-25);
        shot[4].scale(shot[4].getWidth()-55, shot[4].getHeight()-55);
        shot[5].scale(shot[5].getWidth()-35, shot[5].getHeight()-30);
        shot[6].scale(shot[6].getWidth()-20, shot[6].getHeight()-20);
        shot[7].scale(shot[7].getWidth()-10, shot[7].getHeight()-10);
    }
    /**
     * Act - do whatever the Shots wants to do. This method is called whenever
     * the 'Act' or 'Run' button gets pressed in the environment.
     */
    public void act() 
    {
        // Add your action code here.
        remove();
        
    }
    /**
     * Rimuovi l'oggetto dopo un tot di distanza dal Pg
     */
    private void remove(){
        if(getObjectsInRange(320,Pg.class).size()<=0){
            getWorld().removeObject(this);
        }
    }

    /**
     * Modificare lo shot che deve avere il PG - da 0 a 7
     */
    public void changeShot(int choice){
        choiceShot=choice;
    }
    
    /**
     * Overload della funzione getOneIntersectingObject in modo che sia
     * visibile anche da altre classi
     */
    public Actor getOneIntersectingObject(Class c){
        return super.getOneIntersectingObject(c);
    }
}
