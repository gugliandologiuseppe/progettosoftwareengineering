import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)
import java.util.List;
/**
 * Write a description of class SecretMountain1 here.
 * 
 * @author (your name) 
 * @version (a version number or a date)
 */
public class SecretMountain1 extends SWorld
{
    private GreenfootImage bg;
    /**
     * Constructor for objects of class SecretMountain1.
     * 
     */
    public SecretMountain1()
    {    
        // Create a new world with 600x400 cells with a cell size of 1x1 pixels.
        super(850, 600, 1, 1500, 1000); 
        prepare();
        super.act(); 
    }
    /**
     * Prepare the world for the start of the program.
     * That is: create the initial objects and add them to the world.
     */
    private void prepare(){
        oggSfondo();
        
        setMainActor(new Pg(), 300, 410);
        mainActor.setLocation(870, -200);    //150 390
        bg = new GreenfootImage("rock2.png");
        setScrollingBackground(bg);
    }
    public void act(){
        scrollObjects();
        scrollBackground();
        pause();
        nextWorld();
        respawnFallPg();
        
    }
    /**
     * Classe che porta al mondo successivo
    */  
    private void nextWorld(){
        List<Pg> PgList = getObjects(Pg.class);
        Pg pg = PgList.get(0);       
        if(pg.atRightEdge()){
            Greenfoot.setWorld(new Mountain2());
        }
    } 
    /**
     * Il pg prende danno da caduta e ritorna ad inizio livello.
     */
    private void respawnFallPg(){
        List<Pg> PgList = getObjects(Pg.class);
        Pg pg = PgList.get(0);
        if(pg.fallVoid()){
            mainActor.setLocation(800, 100);
        }
    }
    /**
     * Oggetti creati per lo sfondo
     */
    private void oggSfondo(){
        BaseErba baseErba2 = new BaseErba(1);
        addObject(baseErba2,500,700);
        BaseErba baseErba = new BaseErba(1);
        addObject(baseErba,800,350);
        BaseErba baseErba3 = new BaseErba(1);
        addObject(baseErba3,100,550);
        BaseErba baseErba4 = new BaseErba(1);
        addObject(baseErba4,1050,500);
        BaseErba baseErba5 = new BaseErba(1);
        addObject(baseErba5,250,250);
        BaseErba baseErba6 = new BaseErba(1);
        addObject(baseErba6,250,250);
        BaseErba baseErba7 = new BaseErba(1);
        addObject(baseErba7,-300,400);
        BaseErba baseErba8 = new BaseErba(1);
        addObject(baseErba8,-200,150);
        BaseErba baseErba9 = new BaseErba(1);
        addObject(baseErba9,250,-70);
        
        Quadrifoglio quadrifoglio1 = new Quadrifoglio();
        addObject(quadrifoglio1,-260,330);
        Cassa cassa = new Cassa();
        addObject(cassa,450,630);
        Cassa cassa5 = new Cassa();
        addObject(cassa5,550,630);
        Cassa cassa2 = new Cassa();
        addObject(cassa2,-200,85);
        Cassa cassa3 = new Cassa();
        addObject(cassa3,190,-130);
        Cassa cassa4 = new Cassa();
        addObject(cassa4,300,-130);
        
        Cartelli cartello = new Cartelli(1);
        addObject(cartello,1100,420);
        Scritte exit = new Scritte(5);
        exit.getImage().scale(exit.getImage().getWidth() -110, exit.getImage().getHeight() -80);
        addObject(exit,1100,400);
    }
}
