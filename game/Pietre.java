import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)

/**
 * Write a description of class Pietre here.
 * 
 * @author (your name) 
 * @version (a version number or a date)
 */
public class Pietre extends Actor
{
    private int MAX_IMAGES = 7;
    private GreenfootImage[] pietre = new GreenfootImage[MAX_IMAGES];
    
    public Pietre(int choice){
        imagesScale();
        switch(choice){
            case 1:
            //pietre[0].scale(pietre[0].getWidth() -60, pietre[0].getHeight() -70);
            setImage(pietre[0]);
            break;
            case 2:
            //pietre[1].scale(pietre[1].getWidth() -20, pietre[1].getHeight() -20);
            setImage(pietre[1]);
            break;
            case 3:
            //pietre[2].scale(pietre[2].getWidth() -60, pietre[2].getHeight() -250);
            setImage(pietre[2]);
            break;
            case 4:
            //pietre[3].scale(pietre[3].getWidth() -60, pietre[3].getHeight() -250);
            setImage(pietre[3]);
            break;
            case 5:
            //pietre[4].scale(pietre[4].getWidth() -60, pietre[4].getHeight() -250);
            setImage(pietre[4]);
            break;
            case 6:
            //pietre[5].scale(pietre[5].getWidth() -60, pietre[5].getHeight() -250);
            setImage(pietre[5]);
            break;
            case 7:
            //pietre[6].scale(pietre[6].getWidth() -60, pietre[6].getHeight() -250);
            setImage(pietre[6]);
            break;
        }
            
        
    }
    private void imagesScale(){
        for (int i=0; i<MAX_IMAGES; i++){
            pietre[i] = new GreenfootImage("pietra_" +(i+1)+ ".png");
        }
        
    }
    /**
     * Act - do whatever the Pietre wants to do. This method is called whenever
     * the 'Act' or 'Run' button gets pressed in the environment.
     */
    public void act() 
    {
        // Add your action code here.
    }    
}
