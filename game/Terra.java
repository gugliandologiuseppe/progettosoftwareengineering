import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)

/**
 * Write a description of class Terra here.
 * 
 * @author (your name) 
 * @version (a version number or a date)
 */
public class Terra extends Actor
{
    //private GreenfootImage terra;
    private int MAX_IMAGES = 4;
    private GreenfootImage[] terra = new GreenfootImage[MAX_IMAGES];
    public Terra(int choice){
        images();
        switch(choice){
            case 1:
            terra[0].scale(terra[0].getWidth() - 2900, terra[0].getHeight() -300);
            setImage(terra[0]);
            break;
            case 2:
            terra[1].scale(terra[1].getWidth() -200, terra[1].getHeight() -100);
            setImage(terra[1]);
            break;
            case 3:
            terra[2].scale(terra[2].getWidth() -2900, terra[2].getHeight() -300);
            setImage(terra[2]);
            break;
            case 4:
            terra[3].scale(terra[3].getWidth() -2900, terra[3].getHeight() -300);
            setImage(terra[3]);
            break;
        }
        //terra = new GreenfootImage("terra.png");
    }
    private void images(){
        for (int i=0; i<MAX_IMAGES; i++){
            terra[i] = new GreenfootImage("terra_" +(i+1)+ ".png");
        }
        
    }
    /**
     * Act - do whatever the Terra wants to do. This method is called whenever
     * the 'Act' or 'Run' button gets pressed in the environment.
     */
    public void act() 
    {
        // Add your action code here.
    }    
}
