import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)

/**
 * Write a description of class ItemSell here.
 * 
 * @author (your name) 
 * @version (a version number or a date)
 * Gli oggetti dall' 1 al 4 sono potenziamenti, da 5 a 11 shot.
 */
public class ItemSell extends Actor
{
    private int MAX_IMAGES = 11;
    private GreenfootImage[] itemSell = new GreenfootImage[MAX_IMAGES];
    public static int price=0;
    public static int numItem=0;
    
    public ItemSell(int choice){
        images();
        numItem=choice;
        switch(choice){
            case 1:
            itemSell[0].scale(itemSell[0].getWidth() +25, itemSell[0].getHeight() +25);
            setImage(itemSell[0]);
            price=10;
            break;
            case 2:
            setImage(itemSell[1]);
            price=5;
            break;
            case 3:
            itemSell[2].scale(itemSell[2].getWidth() +20, itemSell[2].getHeight() +20);
            setImage(itemSell[2]);
            price=10;
            break;
            case 4:
            setImage(itemSell[3]);
            price=5;
            break;
            case 5:
            itemSell[4].scale(itemSell[4].getWidth() +10, itemSell[4].getHeight() +10);
            setImage(itemSell[4]);
            price=15;
            break;
            case 6:
            itemSell[5].scale(itemSell[5].getWidth() +10, itemSell[5].getHeight() +10);
            setImage(itemSell[5]);
            price=15;
            break;
            case 7:
            itemSell[6].scale(itemSell[6].getWidth() -45, itemSell[6].getHeight() -45);
            setImage(itemSell[6]);
            price=15;
            break;
            case 8:
            itemSell[7].scale(itemSell[7].getWidth() -60, itemSell[7].getHeight() -60);
            setImage(itemSell[7]);
            price=15;
            break;
            case 9:
            itemSell[8].scale(itemSell[8].getWidth() -45, itemSell[8].getHeight() -45);
            setImage(itemSell[8]);
            price=20;
            break;
            case 10:
            itemSell[9].scale(itemSell[9].getWidth() -60, itemSell[9].getHeight() -60);
            setImage(itemSell[9]);
            price=30;
            break;
            case 11:
            itemSell[10].scale(itemSell[10].getWidth() -15, itemSell[10].getHeight() -15);
            setImage(itemSell[10]);
            price=20;
            break;
        }
    }
    private void images(){
        for (int i=0; i<MAX_IMAGES; i++){
            itemSell[i] = new GreenfootImage("sell_" +(i+1)+ ".png");
            
        }
        
    }
    /**
     * Act - do whatever the ItemSell wants to do. This method is called whenever
     * the 'Act' or 'Run' button gets pressed in the environment.
     */
    public void act() 
    {
        // Add your action code here.
    }  
    /**
     * ritorna il prezzo dell'oggetto
     */
    public int priceObject(){
        return price;
    }
    /**
     * ritorna il numero dell'ggetto scelto
     */
    public int numItems(){
        return numItem;
    }
}
