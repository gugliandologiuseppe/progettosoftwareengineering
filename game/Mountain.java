import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)
import java.util.List;
/**
 * Write a description of class AridMountain here.
 * 
 * @author (your name) 
 * @version (a version number or a date)
 */
public class Mountain extends SWorld
{
    private GreenfootImage bg;
    /**
     * Constructor for objects of class AridMountain.
     * 
     */
    public Mountain()
    {    
        // Create a new world with 600x400 cells with a cell size of 1x1 pixels.
        super(850, 600, 1, 2500, 1000); 
        prepare();
        super.act(); 
    }
    /**
     * Prepare the world for the start of the program.
     * That is: create the initial objects and add them to the world.
     */
    private void prepare(){ 
        oggSfondo();
        bloccaStrada();
        
        Checkpoint checkpoint = new Checkpoint(1, "Mountain");
        addObject(checkpoint,100,640);
        
        setMainActor(new Pg(), 100, 410);
        mainActor.setLocation(100, 612);    //150 390
        bg = new GreenfootImage("rock2.png");
        setScrollingBackground(bg);
        
        Alberi albero = new Alberi(5);
        addObject(albero,-200,450);
        
        Slime slime = new Slime();
        addObject(slime,600,630);
        Bat bat = new Bat();
        addObject(bat,1000,200);
    }
    public void act(){
        scrollObjects();
        scrollBackground();
        pause();
        nextWorld();
        secretWorld();
        stopWall();
        
    }
    /**
     * Classe che porta al mondo successivo
    */  
    private void nextWorld(){
        List<Pg> PgList = getObjects(Pg.class);
        Pg pg = PgList.get(0);       
        if(pg.atRightEdge()){
            Greenfoot.setWorld(new Mountain2());
        }
    } 
    /**
     * Classe che porta al mondo successivo
    */  
    private void secretWorld(){
        List<Pg> PgList = getObjects(Pg.class);
        Pg pg = PgList.get(0);       
        if(pg.atLeftEdge()){
            Greenfoot.setWorld(new SecretMountain1());
        }
    }
    /**
     * si ferma alla parente tornando indietro. Questo viene fatto con la linea trasparente 'PerInterazioni'
     */
    private void stopWall(){
        List<Pg> PgList = getObjects(Pg.class);
        Pg pg = PgList.get(0);
        if(pg.PgBarra()>0){
            pg.setLocation(pg.getX()+30,pg.getY());
            
        }
    }
    /**
     * Oggetti creati per lo sfondo
     */
    private void oggSfondo(){
        Terra terra5 = new Terra(2);
        terra5.getImage().scale(terra5.getImage().getWidth() +300, terra5.getImage().getHeight() +250);
        addObject(terra5,-610,400);
        
        Terra terra = new Terra(4);
        addObject(terra,140,663);
        Terra terra2 = new Terra(4);
        addObject(terra2,860,663);
        Terra terra3 = new Terra(4);
        addObject(terra3,1580,663);
        Terra terra4 = new Terra(4);
        addObject(terra4,-570,663);
        
        Cespugli cespuglio = new Cespugli(6);
        addObject(cespuglio,410,580);
        Cespugli cespuglio2 = new Cespugli(6);
        addObject(cespuglio2,1200,580);
        Alberi albero2 = new Alberi(4);
        albero2.getImage().scale(albero2.getImage().getWidth() +20, albero2.getImage().getHeight() +50);
        addObject(albero2,600,410);
        Alberi albero3 = new Alberi(6);
        addObject(albero3,1300,435);
        Pietre pietra = new Pietre(5);
        addObject(pietra,-380,560);
        Cespugli cespuglio3 = new Cespugli(6);
        addObject(cespuglio3,-450,40);
        OggettiVari freccia = new OggettiVari(7);
        addObject(freccia,-750,70);
        OggettiVari freccia2 = new OggettiVari(7);
        freccia2.getImage().mirrorHorizontally();
        addObject(freccia2,-790,70);
        
        BaseErba baseErba2 = new BaseErba(1);
        addObject(baseErba2,900,290);
        BaseErba baseErba = new BaseErba(1);
        addObject(baseErba,464,470);
        BaseErba baseErba3 = new BaseErba(1);
        addObject(baseErba3,464,70);
        BaseErba baseErba4 = new BaseErba(1);
        addObject(baseErba4,144,70);
        BaseErba baseErba5 = new BaseErba(1);
        addObject(baseErba5,1200,70);
        BaseErba baseErba6 = new BaseErba(1);
        addObject(baseErba6,1700,300);
        
        Cassa cassa = new Cassa();
        addObject(cassa,-620,70);
        Quadrifoglio quadrifoglio = new Quadrifoglio();
        addObject(quadrifoglio,1620,230);
        Cartelli cartello = new Cartelli(1);
        addObject(cartello,1565,573);
        
        Spikes spike = new Spikes(1);
        spike.turn(180);
        addObject(spike, 200,25);
        Spikes spike2 = new Spikes(1);
        spike2.turn(180);
        addObject(spike2, 500,25);
    }
    private void bloccaStrada(){
        PerInterazioni linea = new PerInterazioni();
        linea.turn(90);
        addObject(linea,-190,600);
        PerInterazioni linea2 = new PerInterazioni();
        linea2.turn(90);
        addObject(linea2,-190,500);
        PerInterazioni linea3 = new PerInterazioni();
        linea3.turn(90);
        addObject(linea3,-190,300);
        PerInterazioni linea4 = new PerInterazioni();
        linea4.turn(90);
        addObject(linea4,-190,400);
        PerInterazioni linea5 = new PerInterazioni();
        linea5.turn(90);
        addObject(linea5,-190,200);
        PerInterazioni linea6 = new PerInterazioni();
        linea6.turn(90);
        addObject(linea6,-190,100);
    }
}
