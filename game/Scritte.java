import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)
import java.io.File;
import com.google.gson.Gson;
import com.google.gson.stream.JsonReader;
import java.io.FileReader;

/**
 * Write a description of class Scritte here.
 * 
 * @author (your name) 
 * @version (a version number or a date)
 * Choice Scritte:
 * 1-Start
 * 2-Tutorial movimento Pg
 * 3-Tutoria attacco Pg
 * 4-Select Level
 * 5-Exit
 * 6-Restart
 * 7-Start Menu
 * 8-Resume
 * 9-Next Level
 * 10-Restart
 */
public class Scritte extends Actor
{
    private int MAX_IMAGES = 10;
    private GreenfootImage[] scritta = new GreenfootImage[MAX_IMAGES];
    private int number=0;
    
    public Scritte(int choice){
        imagesScale();
        number=choice;
        switch(choice){
            case 1:
            //scritta[0].scale(scritta[0].getWidth(), scritta[0].getHeight());
            setImage(scritta[0]);
            break;
            case 2:
            scritta[1].scale(scritta[1].getWidth()-80, scritta[1].getHeight()-80);
            setImage(scritta[1]);
            break;
            case 3:
            scritta[2].scale(scritta[2].getWidth()-80, scritta[2].getHeight()-80);
            setImage(scritta[2]);
            break;
            case 4:
            setImage(scritta[3]);
            break;
            case 5:
            setImage(scritta[4]);
            break;
            case 6:
            setImage(scritta[5]);
            break;
            case 7:
            setImage(scritta[6]);
            break;
            case 8:
            setImage(scritta[7]);
            break;
            case 9:
            setImage(scritta[8]);
            break;
            case 10:
            setImage(scritta[9]);
            break;
        }
            
    }
    private void imagesScale(){
        for (int i=0; i<MAX_IMAGES; i++){
            scritta[i] = new GreenfootImage("scritta_" +(i+1)+ ".png");
        }
        
    }
    /**
     * Act - do whatever the Scritte wants to do. This method is called whenever
     * the 'Act' or 'Run' button gets pressed in the environment.
     */
    public void act() 
    {
        // Add your action code here.
        if(Greenfoot.mouseClicked(this) && getImage()==scritta[0]){
            salvataggioPg();
        } 
        if(Greenfoot.mouseClicked(this) && getImage()==scritta[3]){
            resetPg();
            Greenfoot.setWorld(new SelectLevel());
        }
        if(Greenfoot.mouseClicked(this) && getImage()==scritta[6]){
            Greenfoot.setWorld(new StartMenu());
        }
        if(Greenfoot.mouseClicked(this) && getImage()==scritta[7]){
            resume();
        }
        if(Greenfoot.mouseClicked(this) && getImage()==scritta[4]){
            //chiusura gioco
            System.exit(0);
        }
        if(Greenfoot.mouseClicked(this) && getImage()==scritta[5]){
            salvataggioPg();
        } 
    } 
    /**
     * 
     
    private void world_next_Start(){
        if(Greenfoot.mouseClicked(this) && number == 1){ 
            //getWorld().showText("Cose2: ", 100, 300); 
            Greenfoot.setWorld(new Forest());
        } 
    }*/
    /**
     * Ritornare alla schermata precedente dopo la Pausa
     */
    public void resume(){
    World gameworld;
    gameworld = getWorld();
        if (Greenfoot.mouseClicked(this)){
            PauseMenu pausemenu = (PauseMenu)getWorld(); 
            Greenfoot.setWorld(pausemenu.getLastWorld());
        }
    }
    /**
     * reset del personaggio dopo la morte, in questo caso resettiamo tutto per permettere un Restart da capo 
     * vita=3, collezionabili= 0, e monete=0; score =0 e shot iniziale
     */
    private void resetPg(){
        Pg pg=new Pg();
        pg.resetPg(3.0,0,0,0,0);
    }
    
    private void salvataggioPg() {
        Pg pg = new Pg();
        try {
            File tempFile = new File("datiCheckpoint.json");
            if(tempFile.exists()) {
                Gson gson = new Gson();
                JsonReader reader = new JsonReader(new FileReader("datiCheckpoint.json"));
                CheckpointData readCase = gson.fromJson(reader, CheckpointData.class);
                pg.checkpointData = readCase;
            }
                
            String mondo = pg.checkpointData == null ? null : pg.checkpointData.checkpoint;
            
            if(mondo == null) {
                resetPg();
                Greenfoot.setWorld(new Forest());
                return;
            }
        
            Greenfoot.setWorld((SWorld) Class.forName(mondo).newInstance());
            pg.resetPgFromCheckpointData();
        } catch(Exception ex) {
            resetPg();
            Greenfoot.setWorld(new Forest());
        }
    }
}
