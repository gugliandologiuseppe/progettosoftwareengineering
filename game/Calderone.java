import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)

/**
 * Write a description of class Calderone here.
 * 
 * @author (your name) 
 * @version (a version number or a date)
 */
public class Calderone extends Actor
{
    private int MAX_IMAGES = 10;
    private GreenfootImage[] calderone = new GreenfootImage[MAX_IMAGES];
    private SimpleTimer timer = new SimpleTimer();
    private boolean timerRunning = false;
    
    public Calderone(){
        imagesScale(); 
        setImage(calderone[0]);
        
    }
    private void imagesScale(){
        for (int i=0; i<MAX_IMAGES; i++){
            calderone[i] = new GreenfootImage("calderone_" +(i+1)+ ".png");
            calderone[i].scale(calderone[i].getWidth() +20, calderone[i].getHeight() +40);
        }
        
    }
    /**
     * Act - do whatever the Calderone wants to do. This method is called whenever
     * the 'Act' or 'Run' button gets pressed in the environment.
     */
    public void act() 
    {
        // Add your action code here.
        animation();
    }    
    /**
    * 
    */
    private void animation(){
        if(timerRunning == false){
            timer.mark();
            timerRunning = true;
        }
        if(timer.millisElapsed() > 230 && timerRunning == true ){
            
            if(getImage()== calderone[0]){
                setImage(calderone[1]);
            }else if(getImage()== calderone[1]){
                setImage(calderone[2]);
            }else if(getImage()== calderone[2]){
                setImage(calderone[3]);
            }else if(getImage()== calderone[3]){
                setImage(calderone[4]);
            }else if(getImage()== calderone[4]){
                setImage(calderone[5]);
            }else if(getImage()== calderone[5]){
                setImage(calderone[6]);
            }else if(getImage()== calderone[6]){
                setImage(calderone[7]);
            }else if(getImage()== calderone[8]){
                setImage(calderone[9]);
            }else {
                setImage(calderone[0]);
            }
            
            timer.mark();
            timerRunning = false; 
        }
    }
}
