import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)
import java.util.List;
/**
 * Write a description of class LevelComplete here.
 * 
 * @author (your name) 
 * @version (a version number or a date)
 */
public class LevelComplete extends World
{
    GreenfootImage bg = new GreenfootImage("sfondo_3.png");
    GreenfootImage cartello = new GreenfootImage("CompleteWorld.png");
    GreenfootImage bg2 = new GreenfootImage("sfondo_4.png");
    Scritte startMenu = new Scritte(7);
    Scritte nextLevel = new Scritte(9);
    private int scorePg =0;
    private int collectiblePg=0;
    private int previewLevel=0;
    PerInterazioni barra = new PerInterazioni();
    /**
     * Constructor for objects of class LevelComplete.
     * 
     */
    public LevelComplete(int score, int collectible, int level)
    {    
        // Create a new world with 600x400 cells with a cell size of 1x1 pixels.
        super(850, 600, 1);
        scorePg=score;
        collectiblePg=collectible;
        previewLevel=level;
        prepare();
    }
    private void prepare(){
        bg.scale(bg.getWidth()+100, bg.getHeight()-300);
        setBackground(bg);
        bg2.scale(bg2.getWidth()-1450, bg2.getHeight()-1320);
        bg.drawImage(bg2,-10,-10);
        
        cartello.scale(cartello.getWidth()+250, cartello.getHeight()+300);
        getBackground().drawImage(cartello, 200, -5);
         
        startMenu.getImage().scale(startMenu.getImage().getWidth()-110, startMenu.getImage().getHeight()-70);
        addObject(startMenu,440,365);
        nextLevel.getImage().scale(nextLevel.getImage().getWidth()+70, nextLevel.getImage().getHeight()+25);
        addObject(nextLevel,440,470);
        
        barra.getImage().scale(barra.getImage().getWidth()+30, barra.getImage().getHeight()+40);
        addObject(barra,440,470);
    }
    public void act(){
        showText(" " +  scorePg, 467, 210);
        showText(" " +  collectiblePg, 380, 280);
        
        if(Greenfoot.mouseClicked(barra) && previewLevel == 1){
            Greenfoot.setWorld(new Mountain());
        }
    }
}
