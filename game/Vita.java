import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)
import java.util.List;

/**
 * Write a description of class Vita here.
 * 
 * @author (your name) 
 * @version (a version number or a date)
 */
public class Vita extends Actor
{
    private int MAX_IMAGES = 6;
    private GreenfootImage[] hearts = new GreenfootImage[MAX_IMAGES];
    private SimpleTimer timer = new SimpleTimer();
    private boolean timerRunning = false;
    private SimpleTimer timer2 = new SimpleTimer();
    private boolean timer2Running = false;
    
    public Vita(){
        for (int i=0; i<MAX_IMAGES; i++){
            hearts[i] = new GreenfootImage("heart_" +(i+1)+ ".png");
            hearts[i].scale(hearts[i].getWidth()-80, hearts[i].getHeight()-65);
        }
        setImage(hearts[0]);
    }
    
    /**
     * Act - do whatever the Vita wants to do. This method is called whenever
     * the 'Act' or 'Run' button gets pressed in the environment.
     */
    public void act() 
    {
        animateheart();
        increaseLifePg();
    }  
    /**
     * 
     */
    private void animateheart(){
        //delayheart
        if(timerRunning == false){
            timer.mark();
            timerRunning = true;
        }
        if(timer.millisElapsed() > 230 && timerRunning == true ){
            
            if(getImage()== hearts[0]){
                setImage(hearts[1]);
            }else if(getImage()== hearts[1]){
                setImage(hearts[2]);
            }else if(getImage()== hearts[2]){
                setImage(hearts[3]);
            }else if(getImage()== hearts[3]){
                setImage(hearts[4]);
            }else if(getImage()== hearts[4]){
                setImage(hearts[5]);
            }else{
                setImage(hearts[0]);
            }
            
            timer.mark();
            timerRunning = false; 
        }
    }
        /**
     * aumenta il valore delle monete che il Pg possiede
     */
    private void increaseLifePg(){
        List<Pg> PgList = getObjectsInRange(30,Pg.class);   
        if (PgList.size()>0){
            if(timer2Running == false){
                timer2.mark();
                timer2Running = true;
            }
            if(timer.millisElapsed() > 200 && timerRunning == true ){
                Pg pg = PgList.get(0);
                pg.increaseLife(1.0);
                getWorld().removeObject(this);
                
                timer2.mark();
                timer2Running = false; 
            }
        }
    }
}
