import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)

/**
 * Write a description of class BaseErba here.
 * 
 * @author (your name) 
 * @version (a version number or a date)
 */
public class BaseErba extends Actor
{
    private GreenfootImage base;
    private int type;
    
    public BaseErba(int choice){
        type=choice;
        if(type== 1){
            base = new GreenfootImage("base2.png");
        }else{
            base = new GreenfootImage("base1.png");
        }
        base.scale(base.getWidth() - 400, base.getHeight() -100);
        setImage(base);
        
    }
    /**
     * Act - do whatever the BaseErba wants to do. This method is called whenever
     * the 'Act' or 'Run' button gets pressed in the environment.
     */
    public void act() 
    {
        // Add your action code here.
    }    
}
